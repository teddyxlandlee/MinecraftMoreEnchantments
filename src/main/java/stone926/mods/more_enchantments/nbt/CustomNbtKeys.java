package stone926.mods.more_enchantments.nbt;

import stone926.mods.more_enchantments.MoreEnchantmentsMod;

public final class CustomNbtKeys {

  private static String s(String s) {
    return MoreEnchantmentsMod.addIdPrefix(s);
  }

  public static final class GUARDIAN {

    public static final String WARMUP_TIME = s("warmupTime");

  }

  public static final class MAGMA_CUBE {

    public static final String LOW_JUMP = s("lowJump");
    public static final String PREVENT_SPLIT_SIZE = s("preventSplitSize");
    public static final String ATTACK_SPEED = ("slimeAttackSpeed");

  }

  public static final class LIGHTNING {

    public static final String OWNER = s("owner");

  }

  public static final class PERSISTENT_PROJECTILE {

    public static final String DISAPPEAR_COUNTDOWN = s("disappearCountdown");
    public static final String TRACTION = s("traction");
    public static final String ENTITY_DAMAGE = s("entityDamage");
    public static final String ENTITY_NOT_DAMAGE = s("entityNotDamage");
    public static final String FROM_ENCH = s("isFromEnch");
    public static final String FORCE_DAMAGE = s("forceDamage");

  }

  public static final class TRIDENT {

    public static final String EXTRA_DAMAGE = s("extraDamage");

  }

  public static final class AREA_EFFECT_CLOUD {

    public static final String ENTITY_DO_NOT_AFFECT = s("doNotAffectUuid");

  }

  public static final class FIREWORK_ROCKET {

    public static final String EXTRA_DAMAGE = s("extraDamage");

  }

  public static final class LIVING_ENTITY {

    public static final String DROPS_NOTHING = s("dropsNothing");
    public static final String HEALED_BY_CORNEL_COOLDOWN = s("healedByCornelCooldown");
    public static final String BLOCK_COOLDOWN = s("blockCooldown");
    public static final String FATE_COOLDOWN = s("fateCooldown");
    public static final String TP_SUPPRESSION = s("tpSuppression");
    public static final String FROM_BLOCK_MANDALA = s("fromBlackMandala");
    public static final String CREEPER_DESTRUCTION_TYPE_NONE = s("destructionTypeNone");
    public static final String VAMPIRE = s("vampire");

  }

  public static final class MOB {

    public static final String TIE = s("tie");
    public static final String AI_DISABLE_TICKS = s("aiDisableTicks");

  }

  public static final class PLAYER_BLAZE {

    public static final String SHOOT_COUNT = s("shootCount");
    public static final String SHOOT_COOLDOWN = s("shootCooldown");
    public static final String SHOULD_SHOOT = s("shouldShoot");
    public static final String BOOST = s("boost");

  }

  public static final class SHULKER_BULLET {

    public static final String EXTRA_DAMAGE = s("extraDamage");
    public static final String FLASH = s("flash");
    public static final String LEVITATION_AMPLIFIER = s("levitationAmplifier");

  }

  public static final class TNT {

    public static final String POWER = s("power");
    public static final String DESTRUCTION_TYPE = s("destructionType");
    public static final String SUMMONED_BY_EXPLOSION_CREATOR = s("summonedByExplosionCreator");

  }

  public static final class WITHER_SKULL {

    public static final String DRAG_MODIFIED = s("dragModified");
    public static final String DRAG = s("drag");
    public static final String EXTRA_DAMAGE = s("extraDamage");
    public static final String STRENGTHENED = s("strengthened");

  }

  public static final class WITCH {

    public static final String STRENGTHENED = s("strengthened");

  }

}
