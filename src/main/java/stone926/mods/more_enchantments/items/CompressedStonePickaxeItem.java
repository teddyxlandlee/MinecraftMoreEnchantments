package stone926.mods.more_enchantments.items;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;
import stone926.mods.more_enchantments.util.RandomUtil;

public class CompressedStonePickaxeItem extends ToolItem {

  public CompressedStonePickaxeItem(Settings settings) {
    super(CompressedStoneMaterial.get(), settings);
  }

  public boolean postMine(ItemStack stack, @NotNull World world, BlockState state, BlockPos pos, LivingEntity miner) {
    if (!world.isClient && state.getHardness(world, pos) != 0.0F) {
      stack.damage(RandomUtil.nextInt(1, 2, miner.getRandom()), miner, (e) -> e.sendEquipmentBreakStatus(EquipmentSlot.MAINHAND));
    }
    return true;
  }

}
