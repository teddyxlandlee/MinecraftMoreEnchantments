package stone926.mods.more_enchantments.items.bow;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.WitherSkullEntity;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.enchantments.WitherSkullShooterEnchantment;
import stone926.mods.more_enchantments.interfaces.IWitherSkull;
import stone926.mods.more_enchantments.util.RandomUtil;

import java.util.function.Predicate;

public class WitherSkullBow extends BowItem implements StoneBow {

  public WitherSkullBow(Settings settings) {
    super(settings);
  }

  @Override
  public void onStoppedUsing(ItemStack bowStack, World world, LivingEntity user, int remainingUseTicks) {
    if (user instanceof PlayerEntity playerEntity) {
      boolean isCreative = playerEntity.getAbilities().creativeMode;
      ItemStack arrowStack = playerEntity.getArrowType(bowStack);
      if (!arrowStack.isEmpty() || isCreative) {
        if (arrowStack.isEmpty()) {arrowStack = new ItemStack(MoreEnchantmentsMod.WITHER_SKULL_GUNPOWDER);}
        int useTime = this.getMaxUseTime(bowStack) - remainingUseTicks;
        float pullProgress = BowItem.getPullProgress(useTime);
        if (pullProgress >= 0.2) {
          WitherSkullEntity witherSkull = WitherSkullShooterEnchantment.getWitherSkull(
            EnchantmentHelper.getLevel(MoreEnchantmentsMod.WITHER_SKULL_SHOOTER, bowStack),
            user, world, pullProgress, EnchantmentHelper.getLevel(MoreEnchantmentsMod.FLASH, bowStack)
          );
          if (world.spawnEntity(witherSkull)) {
            int infiniteLvl = EnchantmentHelper.getLevel(MoreEnchantmentsMod.INFINITE, bowStack);
            if (infiniteLvl <= 2 && !isCreative) {
              arrowStack.decrement(1);
              if (arrowStack.isEmpty()) {playerEntity.getInventory().removeOne(arrowStack);}
            }
            bowStack.damage(RandomUtil.isPossible(pullProgress, user.getRandom()) ? 1 : 3, playerEntity, p -> p.sendToolBreakStatus(playerEntity.getActiveHand()));
          }
        }
      }
    }
  }

  @Override
  public Predicate<ItemStack> getProjectiles() {
    return stack -> stack.isOf(MoreEnchantmentsMod.WITHER_SKULL_GUNPOWDER);
  }

  @Override
  public int getMaxUseTime(ItemStack stack) {
    return 70000;
  }

  @Override
  public void mobUse(MobEntity user, LivingEntity target, ItemStack stack, float pullProgress, ItemStack usingStack) {
    int lvl = EnchantmentHelper.getLevel(MoreEnchantmentsMod.WITHER_SKULL_SHOOTER, usingStack);
    for (int i = 0; i < 3 + user.getRandom().nextInt(6 + lvl); i++) {
      WitherSkullEntity witherSkull = WitherSkullShooterEnchantment.getWitherSkull(
        lvl, user, user.getEntityWorld(), 1, EnchantmentHelper.getLevel(MoreEnchantmentsMod.FLASH, stack)
      );
      witherSkull.setPosition(
        witherSkull.getX(),
        witherSkull.getY() + 0.3 * i * (user.getRandom().nextInt(3) == 0 ? -1 : 1),
        witherSkull.getZ()
      );
      ((IWitherSkull) witherSkull).setExtraDamage(((IWitherSkull) witherSkull).getExtraDamage() + 3);
      if (RandomUtil.isPossible(0.4 * lvl, user.getRandom())) ((IWitherSkull) witherSkull).setStrengthened(true);
      user.getEntityWorld().spawnEntity(witherSkull);
    }
  }

}
