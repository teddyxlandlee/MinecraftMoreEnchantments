package stone926.mods.more_enchantments.items.bow;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.ItemStack;

public interface StoneBow {

  void mobUse(MobEntity user, LivingEntity target, ItemStack stack, float pullProgress, ItemStack usingStack);

}
