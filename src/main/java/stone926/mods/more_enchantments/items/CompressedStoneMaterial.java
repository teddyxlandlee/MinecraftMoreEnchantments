package stone926.mods.more_enchantments.items;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolMaterial;
import net.minecraft.recipe.Ingredient;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

public class CompressedStoneMaterial implements ToolMaterial {

  private static final ToolMaterial COMPRESSED_STONE = new CompressedStoneMaterial();

  private CompressedStoneMaterial() {super();}

  public static ToolMaterial get() {
    return COMPRESSED_STONE;
  }

  @Override
  public int getDurability() {
    return 200;
  }

  @Override
  public float getMiningSpeedMultiplier() {
    return 3;
  }

  @Override
  public float getAttackDamage() {
    return 0;
  }

  @Override
  public int getMiningLevel() {
    return 4;
  }

  @Override
  public int getEnchantability() {
    return 20;
  }

  @Override
  public Ingredient getRepairIngredient() {
    return Ingredient.ofStacks(new ItemStack(MoreEnchantmentsMod.COMPRESSED_STONE_ITEM, 3));
  }

}
