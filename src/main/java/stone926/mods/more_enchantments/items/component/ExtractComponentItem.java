package stone926.mods.more_enchantments.items.component;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsage;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

public class ExtractComponentItem extends ComponentItem {

  public ExtractComponentItem(Settings settings) {
    super(settings);
  }

  @Override
  public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
    ItemStack stack = user.getStackInHand(hand);
    if (stack.isOf(MoreEnchantmentsMod.EXTRACT_COMPONENT_HEAD)) {
      return TypedActionResult.success(ItemUsage.exchangeStack(stack, user, new ItemStack(MoreEnchantmentsMod.EXTRACT_COMPONENT_REAR)));
    } else if (stack.isOf(MoreEnchantmentsMod.EXTRACT_COMPONENT_REAR)) {
      return TypedActionResult.success(ItemUsage.exchangeStack(stack, user, new ItemStack(MoreEnchantmentsMod.EXTRACT_COMPONENT_DIRECT)));
    } else if (stack.isOf(MoreEnchantmentsMod.EXTRACT_COMPONENT_DIRECT)) {
      return TypedActionResult.success(ItemUsage.exchangeStack(stack, user, new ItemStack(MoreEnchantmentsMod.EXTRACT_COMPONENT_HEAD)));
    } else return TypedActionResult.pass(stack);
  }

}
