package stone926.mods.more_enchantments.items.component;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsage;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

public class MoveComponentItem extends ComponentItem {

  public MoveComponentItem(Settings settings) {
    super(settings);
  }

  @Override
  public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
    ItemStack stack = user.getStackInHand(hand);
    if (stack.isOf(MoreEnchantmentsMod.MOVE_COMPONENT_BACKWARD)) {
      return TypedActionResult.success(ItemUsage.exchangeStack(stack, user, new ItemStack(MoreEnchantmentsMod.MOVE_COMPONENT_FORWARD)));
    } else if (stack.isOf(MoreEnchantmentsMod.MOVE_COMPONENT_FORWARD)) {
      return TypedActionResult.success(ItemUsage.exchangeStack(stack, user, new ItemStack(MoreEnchantmentsMod.MOVE_COMPONENT_BACKWARD)));
    } else return TypedActionResult.pass(stack);
  }

}


