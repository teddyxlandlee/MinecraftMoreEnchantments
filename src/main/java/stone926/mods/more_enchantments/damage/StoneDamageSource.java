package stone926.mods.more_enchantments.damage;

import net.minecraft.entity.damage.DamageSource;

public class StoneDamageSource extends DamageSource {

  public static DamageSource BLAZE_ENCHANTMENT = new StoneDamageSource("blaze_enchantment").setFire();
  public static DamageSource MULTIPLY_CURSE = new StoneDamageSource("multiply_curse")
    .setUsesMagic()
    .setBypassesArmor()
    .setUnblockable();
  public static DamageSource ENDER_DRAGON_ENCHANTMENT = new StoneDamageSource("ender_dragon_enchantment")
    .setBypassesArmor()
    .setUnblockable();
  public static DamageSource FIREBALL_THROWER_ENCHANTMENT = new StoneDamageSource("fireball_thrower_enchantment")
    .setExplosive()
    .setBypassesArmor();
  public static DamageSource SACRIFICE_ENCHANTMENT = new StoneDamageSource("sacrifice_enchantment")
    .setBypassesArmor()
    .setUnblockable();
  public static DamageSource FATAL_SACRIFICE_ENCHANTMENT = new StoneDamageSource("fatal_sacrifice_enchantment")
    .setBypassesArmor()
    .setUnblockable();
  public static DamageSource CHRYSANTHEMUM = new StoneDamageSource("chrysanthemum").setBypassesArmor();
  public static DamageSource BLACK_MANDALA = new StoneDamageSource("black_mandala").setBypassesArmor();
  public static DamageSource CORNEL = new StoneDamageSource("cornel").setBypassesArmor().setUnblockable();
  public static DamageSource EXPERIENCE_CONVERT = new StoneDamageSource("experience_convert")
    .setUnblockable()
    .setBypassesArmor()
    .setUsesMagic();
  public static DamageSource GOLDEN_ANVIL = new StoneDamageSource("golden_anvil").setFallingBlock().setBypassesArmor();
  public static DamageSource TP_SUPPRESSION = new StoneDamageSource("tp_suppression").setBypassesArmor().setUsesMagic();

  public StoneDamageSource(String name) {
    super(name);
  }

  @Override
  public StoneDamageSource setUnblockable() {
    return (StoneDamageSource) super.setUnblockable();
  }

  @Override
  public StoneDamageSource setBypassesArmor() {
    return (StoneDamageSource) super.setBypassesArmor();
  }

  @Override
  public StoneDamageSource setExplosive() {
    return (StoneDamageSource) super.setExplosive();
  }

  @Override
  public StoneDamageSource setUsesMagic() {
    return (StoneDamageSource) super.setUsesMagic();
  }

  @Override
  public StoneDamageSource setFallingBlock() {
    return (StoneDamageSource) super.setFallingBlock();
  }

}
