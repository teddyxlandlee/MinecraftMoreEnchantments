package stone926.mods.more_enchantments.damage;

import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.EntityDamageSource;
import net.minecraft.entity.player.PlayerEntity;

public class StoneEntityDamageSource extends EntityDamageSource {

  private boolean isExplosionCreator = false;
  private boolean isDecapitation = false;
  private boolean isRetaliation = false;
  private boolean isDeath = false;
  private boolean isVampireEffect = false;
  private boolean isExtinction = false;

  public StoneEntityDamageSource(String name, Entity source) {
    super(name, source);
  }

  public static StoneEntityDamageSource decapitation(Entity source) {
    return new StoneEntityDamageSource("decapitation", source).setUnblockable().setBypassesArmor().setDecapitation();
  }

  public static StoneEntityDamageSource explosionCreator(Entity source) {
    return new StoneEntityDamageSource("explosionCreator", source).setScaledWithDifficulty().setExplosionCreator();
  }

  public static StoneEntityDamageSource extinction(Entity source) {
    return new StoneEntityDamageSource("extinction", source).setUnblockable().setBypassesArmor().setUsesMagic().setExtinction();
  }

  public static StoneEntityDamageSource virtue(Entity source) {
    return new StoneEntityDamageSource("virtue", source).setBypassesArmor();
  }

  public static StoneEntityDamageSource makeAmends(Entity source) {
    return new StoneEntityDamageSource("make_amends", source).setUsesMagic();
  }

  public static StoneEntityDamageSource counterattack(Entity source) {
    return new StoneEntityDamageSource("counterattack", source);
  }

  public static StoneEntityDamageSource retaliation(Entity source) {
    return new StoneEntityDamageSource("retaliation", source).setRetaliation();
  }

  public static StoneEntityDamageSource death(PlayerEntity source) {
    return new StoneEntityDamageSource("death", source).setDeath().setUnblockable().setBypassesArmor().setUsesMagic();
  }

  public static StoneEntityDamageSource vampireEffect(Entity source) {
    return new StoneEntityDamageSource("vampireEffect", source).setUsesMagic().setUnblockable().setBypassesArmor().setVampireEffect();
  }

  @Override
  public StoneEntityDamageSource setUnblockable() {
    return (StoneEntityDamageSource) super.setUnblockable();
  }

  @Override
  public StoneEntityDamageSource setUsesMagic() {
    return (StoneEntityDamageSource) super.setUsesMagic();
  }

  @Override
  public StoneEntityDamageSource setBypassesArmor() {
    return (StoneEntityDamageSource) super.setBypassesArmor();
  }

  @Override
  public StoneEntityDamageSource setExplosive() {
    return (StoneEntityDamageSource) super.setExplosive();
  }

  @Override
  public StoneEntityDamageSource setScaledWithDifficulty() {
    return (StoneEntityDamageSource) super.setScaledWithDifficulty();
  }

  public boolean isExplosionCreator() {
    return isExplosionCreator;
  }

  public boolean isDecapitation() {
    return isDecapitation;
  }

  public boolean isRetaliation() {
    return isRetaliation;
  }

  public boolean isVampireEffect() {
    return isVampireEffect;
  }

  public boolean isDeath() {
    return isDeath;
  }

  public boolean isExtinction() {
    return isExtinction;
  }

  public StoneEntityDamageSource setExtinction() {
    isExtinction = true;
    return this;
  }

  private StoneEntityDamageSource setDeath() {
    isDeath = true;
    return this;
  }

  private StoneEntityDamageSource setDecapitation() {
    isDecapitation = true;
    return this;
  }

  public StoneEntityDamageSource setExplosionCreator() {
    isExplosionCreator = true;
    return this;
  }

  private StoneEntityDamageSource setRetaliation() {
    isRetaliation = true;
    return this;
  }

  private StoneEntityDamageSource setVampireEffect() {
    isVampireEffect = true;
    return this;
  }

}
