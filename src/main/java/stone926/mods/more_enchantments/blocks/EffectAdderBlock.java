package stone926.mods.more_enchantments.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.SimpleNamedScreenHandlerFactory;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import stone926.mods.more_enchantments.screen.EffectAdderScreenHandler;

public class EffectAdderBlock extends Block {

  public EffectAdderBlock(Settings settings) {
    super(settings);
  }

  @SuppressWarnings("deprecation")
  public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
    if (world.isClient) {
      return ActionResult.SUCCESS;
    } else {
      player.openHandledScreen(state.createScreenHandlerFactory(world, pos));
      return ActionResult.CONSUME;
    }
  }

  @Nullable
  @Override
  @SuppressWarnings("deprecation")
  public NamedScreenHandlerFactory createScreenHandlerFactory(BlockState state, World world, BlockPos pos) {
    return new SimpleNamedScreenHandlerFactory(
      (syncId, inventory, player) ->
        new EffectAdderScreenHandler(
          ScreenHandlerType.ANVIL,
          syncId,
          inventory,
          ScreenHandlerContext.create(world, pos)
        ),
      new TranslatableText("title.stone_more_enchantments.effect_adder")
    );
  }

}
