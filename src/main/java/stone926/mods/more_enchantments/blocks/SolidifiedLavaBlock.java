package stone926.mods.more_enchantments.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FrostedIceBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.util.EnchantmentUtil;
import stone926.mods.more_enchantments.util.RandomUtil;

public class SolidifiedLavaBlock extends FrostedIceBlock {

  public SolidifiedLavaBlock(Settings settings) {
    super(settings);
  }

  protected void melt(BlockState state, World world, BlockPos pos) {
    world.setBlockState(pos, Blocks.LAVA.getDefaultState());
    world.updateNeighbor(pos, Blocks.LAVA, pos);
  }

  public void onSteppedOn(World world, BlockPos pos, BlockState state, Entity entity) {
    if (entity instanceof LivingEntity && !EnchantmentUtil.hasEquipmentEnchantment(MoreEnchantmentsMod.SOLIDIFY_WALKER, (LivingEntity) entity)) {
      entity.damage(DamageSource.HOT_FLOOR, RandomUtil.nextInt(2, 4, world.random));
    }

    super.onSteppedOn(world, pos, state, entity);
  }

}
