package stone926.mods.more_enchantments.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.Fertilizable;
import net.minecraft.block.FlowerBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.TargetPredicate;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.Difficulty;
import net.minecraft.world.World;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.damage.StoneDamageSource;
import stone926.mods.more_enchantments.interfaces.IEntitySpawnedByFlower;
import stone926.mods.more_enchantments.util.RandomUtil;

import java.util.Random;

public class ChrysanthemumFlowerBlock extends FlowerBlock {

  public ChrysanthemumFlowerBlock(StatusEffect suspiciousStewEffect, int effectDuration, Settings settings) {
    super(suspiciousStewEffect, effectDuration, settings);
  }

  @Override
  public void onEntityCollision(BlockState state, World world, BlockPos pos, Entity entity) {
    if (!world.isClient && world.getDifficulty() != Difficulty.PEACEFUL) {
      if (entity instanceof LivingEntity livingEntity
        && !livingEntity.hasStatusEffect(MoreEnchantmentsMod.MEMORIAL_CEREMONY_EFFECT)
        && !((IEntitySpawnedByFlower) entity).isSpawnedByMandala()
      ) {
        livingEntity.addStatusEffect(new StatusEffectInstance(
          MoreEnchantmentsMod.MEMORIAL_CEREMONY_EFFECT, 320, 1));
        if (world.random.nextInt(100) == 0)
          ((Fertilizable) this).grow((ServerWorld) world, world.random, pos, state);
      }
      if (entity instanceof HostileEntity
        && world.random.nextInt(10) == 0
        && !((IEntitySpawnedByFlower) entity).isSpawnedByMandala()
      ) {
        world.breakBlock(pos, false);
        entity.damage(StoneDamageSource.CHRYSANTHEMUM, Integer.MAX_VALUE);
        ((Fertilizable) this).grow((ServerWorld) world, world.random, pos, state);
        ServerWorld server = (ServerWorld) world;
        server.spawnParticles(
          ParticleTypes.FIREWORK,
          pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5,
          30,
          0.2, 1, 0.2,
          0
        );
        server.spawnParticles(
          ParticleTypes.SNOWFLAKE,
          entity.getX(), entity.getY() + entity.getHeight(), entity.getZ(),
          30,
          0.2, 0.2, 0.2,
          0.1
        );
      }
    }
  }

  @Override
  public void randomDisplayTick(BlockState state, World world, BlockPos pos, Random random) {
    super.randomDisplayTick(state, world, pos, random);
    RandomUtil.runIfPossible(0.1, random, () -> {
      world.addParticle(ParticleTypes.SNOWFLAKE, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 0.1, 0);
    });
  }

  @Override
  public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
    super.randomTick(state, world, pos, random);
    if (random.nextInt(4) == 0) {
      HostileEntity closestEntity = world.getClosestEntity(
        HostileEntity.class,
        TargetPredicate.createAttackable(),
        null,
        pos.getX(), pos.getY(), pos.getZ(),
        Box.from(new Vec3d(pos.getX(), pos.getY(), pos.getZ())).expand(7)
      );
      if (closestEntity != null && !((IEntitySpawnedByFlower) closestEntity).isSpawnedByMandala()) {
        world.spawnParticles(
          ParticleTypes.SNOWFLAKE,
          closestEntity.getX(), closestEntity.getY() + closestEntity.getHeight(), closestEntity.getZ(),
          30,
          0.2, 0.2, 0.2,
          0.1
        );
        closestEntity.damage(StoneDamageSource.CHRYSANTHEMUM, Integer.MAX_VALUE);
        world.spawnParticles(
          ParticleTypes.FIREWORK,
          pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5,
          30,
          0.2, 1, 0.2,
          0
        );
        if (random.nextInt(5) == 0) {
          world.breakBlock(pos, false);
        }
        ((Fertilizable) this).grow(world, random, pos, state);
      }
    }

    if (random.nextInt(70) == 0)
      ((Fertilizable) this).grow(world, random, pos, state);
  }

  @Override
  public boolean canReplace(BlockState state, ItemPlacementContext context) {
    return context.getStack().isOf(MoreEnchantmentsMod.BLACK_MANDALA_ITEM);
  }

}
