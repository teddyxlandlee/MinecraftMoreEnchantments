package stone926.mods.more_enchantments.blocks.eneity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.interfaces.ImplInventory;

public class EnchantmentOperatorBlockEntity extends BlockEntity implements ImplInventory {

  private static final int SIZE = 6;
  private DefaultedList<ItemStack> inventory = DefaultedList.ofSize(SIZE, ItemStack.EMPTY);

  public EnchantmentOperatorBlockEntity(BlockPos pos, BlockState state) {
    super(MoreEnchantmentsMod.ENCHANTMENT_OPERATOR_BLOCK_ENTITY_TYPE, pos, state);
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return inventory;
  }

  @Override
  public void readNbt(NbtCompound nbt) {
    super.readNbt(nbt);
    inventory = DefaultedList.ofSize(SIZE, ItemStack.EMPTY);
    Inventories.readNbt(nbt, inventory);
  }

  @Override
  protected void writeNbt(NbtCompound nbt) {
    super.writeNbt(nbt);
    Inventories.writeNbt(nbt, inventory);
  }

}
