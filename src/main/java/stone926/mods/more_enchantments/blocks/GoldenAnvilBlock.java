package stone926.mods.more_enchantments.blocks;

import net.minecraft.block.AnvilBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.FallingBlockEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.SimpleNamedScreenHandlerFactory;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.damage.StoneDamageSource;
import stone926.mods.more_enchantments.screen.GoldenAnvilBlockScreenHandler;
import stone926.mods.more_enchantments.util.RandomUtil;

public class GoldenAnvilBlock extends AnvilBlock {

  public GoldenAnvilBlock(Settings settings) {
    super(settings);
  }

  @NotNull
  public static BlockState getLandingState(BlockState fallingState) {
    if (fallingState.isOf(MoreEnchantmentsMod.GOLDEN_ANVIL_BLOCK)) {
      return MoreEnchantmentsMod.CHIPPED_GOLDEN_ANVIL_BLOCK.getDefaultState().with(FACING, fallingState.get(FACING));
    } else {
      return fallingState.isOf(MoreEnchantmentsMod.CHIPPED_GOLDEN_ANVIL_BLOCK) ?
        MoreEnchantmentsMod.DAMAGED_GOLDEN_ANVIL_BLOCK.getDefaultState().with(FACING, fallingState.get(FACING)) :
        Blocks.AIR.getDefaultState();
    }
  }

  public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
    if (world.isClient) {
      return ActionResult.SUCCESS;
    } else {
      player.openHandledScreen(state.createScreenHandlerFactory(world, pos));
      return ActionResult.CONSUME;
    }
  }

  @Override
  protected void configureFallingBlockEntity(FallingBlockEntity entity) {
    entity.setHurtEntities(5, Integer.MAX_VALUE - 1000);
  }

  @Nullable
  public NamedScreenHandlerFactory createScreenHandlerFactory(BlockState state, World world, BlockPos pos) {
    return new SimpleNamedScreenHandlerFactory((syncId, inventory, player) -> new GoldenAnvilBlockScreenHandler(
      ScreenHandlerType.ANVIL, syncId, inventory, ScreenHandlerContext.create(world, pos)
    ), new TranslatableText("title.stone_more_enchantments.golden_anvil"));
  }

  @Override
  public int getFallDelay() {
    return 10;
  }

  @Override
  public void onLanding(World world, BlockPos pos, BlockState fallingBlockState, BlockState currentStateInPos, FallingBlockEntity fallingBlockEntity) {
    super.onLanding(world, pos, fallingBlockState, currentStateInPos, fallingBlockEntity);
    RandomUtil.runIfPossible(0.4, world.random, () -> world.setBlockState(pos, getLandingState(fallingBlockState)));
  }

  @Override
  public void onDestroyedOnLanding(World world, BlockPos pos, FallingBlockEntity fallingBlockEntity) {
    super.onDestroyedOnLanding(world, pos, fallingBlockEntity);
  }

  @Override
  public DamageSource getDamageSource() {
    return StoneDamageSource.GOLDEN_ANVIL;
  }

}
