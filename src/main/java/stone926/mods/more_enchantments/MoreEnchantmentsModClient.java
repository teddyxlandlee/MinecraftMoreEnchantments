package stone926.mods.more_enchantments;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.particle.v1.ParticleFactoryRegistry;
import net.minecraft.client.particle.ElderGuardianAppearanceParticle;
import net.minecraft.client.particle.EmotionParticle;
import net.minecraft.client.particle.EndRodParticle;
import net.minecraft.client.particle.FlameParticle;
import net.minecraft.client.render.RenderLayer;

public class MoreEnchantmentsModClient implements ClientModInitializer {

  @Override
  public void onInitializeClient() {
    /* Adds our particle textures to vanilla's Texture Atlas so it can be shown properly.
     * Modify the namespace and particle id accordingly.
     *
     * This is only used if you plan to add your own textures for the particle. Otherwise, remove  this.*/
//    ClientSpriteRegistryCallback.event(PlayerScreenHandler.BLOCK_ATLAS_TEXTURE).register(((atlasTexture, registry) -> {
//      registry.register(MoreEnchantmentsMod.id("particle/damage_blocked"));
//      registry.register(MoreEnchantmentsMod.id("particle/trident_rain"));
////      registry.register(MoreEnchantmentsMod.id("particle/vampire_heart"));
//    }));

    /* Registers our particle client-side.
     * First argument is our particle's instance, created previously on ExampleMod.
     * Second argument is the particle's factory. The factory controls how the particle behaves.
     * In this example, we'll use FlameParticle's Factory.*/
    ParticleFactoryRegistry.getInstance().register(MoreEnchantmentsMod.DAMAGE_BLOCKED, FlameParticle.Factory::new);
    ParticleFactoryRegistry.getInstance().register(MoreEnchantmentsMod.TRIDENT_RAIN, EndRodParticle.Factory::new);
    ParticleFactoryRegistry.getInstance().register(MoreEnchantmentsMod.VAMPIRE_HEART, EmotionParticle.HeartFactory::new);
    ParticleFactoryRegistry.getInstance().register(MoreEnchantmentsMod.WATER_BREATHING_REMOVED, provider -> new ElderGuardianAppearanceParticle.Factory());

    BlockRenderLayerMap.INSTANCE.putBlock(MoreEnchantmentsMod.BLACK_MANDALA, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(MoreEnchantmentsMod.CHRYSANTHEMUM, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(MoreEnchantmentsMod.CORNEL, RenderLayer.getCutout());
  }

}
