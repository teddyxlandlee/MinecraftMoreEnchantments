package stone926.mods.more_enchantments.spawn;

import net.minecraft.entity.mob.MobEntity;

import java.util.Random;

@FunctionalInterface
public interface EntityInitializer<T extends MobEntity> {

  void initialize(T mob, Random random);

}
