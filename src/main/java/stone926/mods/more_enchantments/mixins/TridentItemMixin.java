package stone926.mods.more_enchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.TridentEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.interfaces.IPersistentProjectileEntity;
import stone926.mods.more_enchantments.mixins.accessors.TridentAccessor;

@Mixin(TridentItem.class)
public class TridentItemMixin extends Item {

  public TridentItemMixin(Settings settings) {
    super(settings);
  }

  @Redirect(method = "onStoppedUsing", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/projectile/TridentEntity;setVelocity(Lnet/minecraft/entity/Entity;FFFFF)V"))
  private void handleFlash(TridentEntity tridentEntity, Entity shooter, float pitch, float yaw, float roll, float speed, float divergence, ItemStack stack) {
    int flashLvl = EnchantmentHelper.getLevel(MoreEnchantmentsMod.FLASH, stack);
    tridentEntity.setVelocity(shooter, pitch, yaw, roll, speed * (flashLvl > 0 ? flashLvl + 1 : 1), divergence);
  }

  @Redirect(method = "onStoppedUsing", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;spawnEntity(Lnet/minecraft/entity/Entity;)Z"))
  private boolean handleTraction(World world, Entity entity) {
    TridentEntity trident = (TridentEntity) entity;
    ((IPersistentProjectileEntity) trident).setTraction(EnchantmentHelper.getLevel(
      MoreEnchantmentsMod.TRACTION, ((TridentAccessor) trident).invokeAsItemStack()));
    return world.spawnEntity(entity);
  }

}
