package stone926.mods.more_enchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.enchantments.DestroyCurseEnchantment;
import stone926.mods.more_enchantments.enchantments.SacrificeEnchantment;
import stone926.mods.more_enchantments.enchantments.creative.FlyEnchantment;
import stone926.mods.more_enchantments.enchantments.creative.UltimateWeaponEnchantment;

import static stone926.mods.more_enchantments.MoreEnchantmentsMod.*;

@Mixin(Item.class)
public abstract class ItemMixin implements ItemConvertible {

  private void handleFly(PlayerEntity user, ItemStack stack) {
    if (user.getAbilities().creativeMode) {
      int flyLvl = EnchantmentHelper.getLevel(FLY, stack);
      if (flyLvl > 0) {
        FlyEnchantment.fly(user, flyLvl);
      }
    }
  }

  private void handleUltimateWeapon(PlayerEntity user, ItemStack stack) {
    if (user.getAbilities().creativeMode) {
      int ultimateWeaponLvl = EnchantmentHelper.getLevel(ULTIMATE_WEAPON, stack);
      if (ultimateWeaponLvl > 0) {
        UltimateWeaponEnchantment.killEntities(
          Box.from(user.getPos())
             .expand(UltimateWeaponEnchantment.getKillingRadius()), user.getEntityWorld(), user);
      }
    }
  }


  @Inject(method = "use", at = @At("HEAD"), cancellable = true)
  public void use(World world, PlayerEntity user, Hand hand, CallbackInfoReturnable<TypedActionResult<ItemStack>> cir) {
    ItemStack stack = user.getStackInHand(hand);
    int sacrificeLvl = EnchantmentHelper.getLevel(SACRIFICE, stack);
    if (sacrificeLvl > 0 && (user.experienceLevel > 0 || user.getAbilities().creativeMode)) {
      boolean sacrificeSucceed = SacrificeEnchantment.killNearByEntities(world, user, sacrificeLvl);
      if (sacrificeSucceed) {
        SacrificeEnchantment.startRite(world, user, sacrificeLvl);
        cir.setReturnValue(TypedActionResult.success(stack));
        return;
      }
    }
    handleFly(user, stack);
    handleUltimateWeapon(user, stack);
    int mowerLvl = EnchantmentHelper.getLevel(MOWER, stack);
    int trimmerLvl = EnchantmentHelper.getLevel(TRIMMER, stack);
    int cultivationLvl = EnchantmentHelper.getLevel(CULTIVATION, stack);
    int fireExtinguisherLvl = EnchantmentHelper.getLevel(FIRE_EXTINGUISHER, stack);
    int destroyCurseLvl = EnchantmentHelper.getLevel(DESTROY_CURSE, stack);
    BlockPos userPos = user.getBlockPos();
    boolean cleanUpSucceed;
    boolean shouldDrop = destroyCurseLvl <= 0;
    cleanUpSucceed = MOWER.cleanUp(userPos, world, user, shouldDrop, mowerLvl, hand);
    cleanUpSucceed = TRIMMER.cleanUp(userPos, world, user, shouldDrop, trimmerLvl, hand) || cleanUpSucceed;
    cleanUpSucceed = CULTIVATION.cleanUp(userPos, world, user, shouldDrop, cultivationLvl, hand) || cleanUpSucceed;
    cleanUpSucceed = FIRE_EXTINGUISHER.cleanUp(userPos, world, user, shouldDrop, fireExtinguisherLvl, hand) || cleanUpSucceed;
    if (cleanUpSucceed) {
      cir.setReturnValue(TypedActionResult.success(stack));
      return;
    }
    boolean destroySucceed = DestroyCurseEnchantment.cleanUp(userPos, world, user, destroyCurseLvl, hand);
    if (destroySucceed) {
      cir.setReturnValue(TypedActionResult.success(stack));
    }
  }

}
