package stone926.mods.more_enchantments.mixins;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.HoeItem;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.ToolItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.text.*;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.enchantments.creative.FallingBlockCreatorEnchantment;

@Mixin(HoeItem.class)
public class HoeItemMixin extends ToolItem {

  public HoeItemMixin(ToolMaterial material, Settings settings) {
    super(material, settings);
  }

  @Inject(method = "useOnBlock", at = @At("HEAD"), cancellable = true)
  private void useOnBlock(ItemUsageContext context, CallbackInfoReturnable<ActionResult> cir) {
    if (EnchantmentHelper.getLevel(MoreEnchantmentsMod.FLY, context.getStack()) > 0) {
      cir.setReturnValue(ActionResult.PASS);
    }
    PlayerEntity contextPlayer = context.getPlayer();
    if (EnchantmentHelper.getLevel(MoreEnchantmentsMod.FALLING_BLOCK_CREATOR, context.getStack()) > 0
      && contextPlayer != null && contextPlayer.getAbilities().creativeMode) {
      BlockPos pos = context.getBlockPos();
      Pair<BlockState, BlockPos> couple = FallingBlockCreatorEnchantment.createFallingBlock(context.getWorld(), pos);
      BlockState state = couple.getLeft();
      BlockPos fallingBlockPos = couple.getRight();

      if (contextPlayer instanceof ClientPlayerEntity cp && !state.isOf(Blocks.AIR)) {
        Text styledUserMsg = new TranslatableText("message.stone_more_enchantments.origin", cp.getDisplayName()).formatted(Formatting.YELLOW);
        Text styledBlockMsg = new TranslatableText("message.stone_more_enchantments.origin", state.getBlock().getName())
          .formatted(Formatting.YELLOW);
        Text position = Texts
          .bracketed(
            new TranslatableText(
              "chat.coordinates", fallingBlockPos.getX(), fallingBlockPos.getY(), fallingBlockPos.getZ()
            ))
          .formatted(Formatting.GREEN)
          .styled((style -> style
            .withClickEvent(new ClickEvent(
              ClickEvent.Action.SUGGEST_COMMAND,
              new StringBuilder()
                .append("/tp @s").append(" ")
                .append(fallingBlockPos.getX()).append(" ")
                .append(fallingBlockPos.getY()).append(" ")
                .append(fallingBlockPos.getZ())
                .toString()
            ))
            .withHoverEvent(new HoverEvent(
              HoverEvent.Action.SHOW_TEXT,
              new TranslatableText("message.stone_more_enchantments.click_to_teleport")
            ))));
        cp.sendSystemMessage(new TranslatableText("message.stone_more_enchantments.create_falling_block", styledUserMsg, styledBlockMsg, position), Util.NIL_UUID);
      }
      cir.setReturnValue(ActionResult.SUCCESS);
    }
  }

}

