package stone926.mods.more_enchantments.mixins;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import stone926.mods.more_enchantments.util.BlockUtil;

@Mixin(ClientPlayerInteractionManager.class)
public class ClientPlayerInteractionManagerMixin {

  @Shadow
  @Final
  private MinecraftClient client;

  @Redirect(method = "breakBlock", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/ClientPlayerEntity;isCreativeLevelTwoOp()Z"))
  private boolean breakBlock(ClientPlayerEntity clientPlayer, BlockPos pos) {
    return clientPlayer.isCreativeLevelTwoOp() || BlockUtil.canUseCompressedStonePickaxe(clientPlayer, client.world.getBlockState(pos));
  }

}
