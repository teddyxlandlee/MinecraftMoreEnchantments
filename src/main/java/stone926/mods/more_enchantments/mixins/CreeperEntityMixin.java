package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.CreeperEntity;
import net.minecraft.world.World;
import net.minecraft.world.explosion.Explosion;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import stone926.mods.more_enchantments.interfaces.ICreeperEntity;

@Mixin(CreeperEntity.class)
public abstract class CreeperEntityMixin extends LivingEntity implements ICreeperEntity {

  private boolean destructionTypeNone = false;

  protected CreeperEntityMixin(EntityType<? extends LivingEntity> entityType, World world) {
    super(entityType, world);
  }

  @ModifyArg(method = "explode", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;createExplosion(Lnet/minecraft/entity/Entity;DDDFLnet/minecraft/world/explosion/Explosion$DestructionType;)Lnet/minecraft/world/explosion/Explosion;"))
  private Explosion.DestructionType explode(Explosion.DestructionType destructionType) {
    if (destructionTypeNone()) return Explosion.DestructionType.NONE;
    return destructionType;
  }

  @Override
  public boolean destructionTypeNone() {
    return destructionTypeNone;
  }

  @Override
  public void setDestructionTypeNone(boolean destructionTypeNone) {
    this.destructionTypeNone = destructionTypeNone;
  }

}
