package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.thrown.EnderPearlEntity;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.hit.HitResult;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import stone926.mods.more_enchantments.damage.StoneDamageSource;
import stone926.mods.more_enchantments.interfaces.ILivingEntity;
import stone926.mods.more_enchantments.util.RandomUtil;

@Mixin(EnderPearlEntity.class)
public abstract class EndPearlEntityMixin extends Entity {

  public EndPearlEntityMixin(EntityType<?> type, World world) {
    super(type, world);
  }

  @Inject(method = "onCollision", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/projectile/thrown/ThrownItemEntity;onCollision(Lnet/minecraft/util/hit/HitResult;)V"), cancellable = true)
  private void onCollision(HitResult hitResult, CallbackInfo ci) {
    Entity entity = ((ProjectileEntity) (Object) this).getOwner();
    if (world instanceof ServerWorld server && entity instanceof LivingEntity owner && ((ILivingEntity) owner).getTpSuppression() > 0) {
      ci.cancel();
      ((ILivingEntity) owner).forceDamage(StoneDamageSource.TP_SUPPRESSION, RandomUtil.nextInt(5, 10, random));
      server.spawnParticles(
        ParticleTypes.CLOUD, getX(), getY(), getZ(), RandomUtil.nextInt(10, 20, random), 0.2, 0.2, 0.2, 0
      );
      discard();
    }
  }

}
