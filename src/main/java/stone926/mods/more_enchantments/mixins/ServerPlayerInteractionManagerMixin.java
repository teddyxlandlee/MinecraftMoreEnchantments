package stone926.mods.more_enchantments.mixins;

import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.network.ServerPlayerInteractionManager;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import stone926.mods.more_enchantments.util.BlockUtil;

@Mixin(ServerPlayerInteractionManager.class)
public class ServerPlayerInteractionManagerMixin {

  @Shadow
  protected ServerWorld world;

  @Redirect(method = "tryBreakBlock", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/network/ServerPlayerEntity;isCreativeLevelTwoOp()Z"))
  private boolean breakBlock(ServerPlayerEntity serverPlayer, BlockPos pos) {
    return serverPlayer.isCreativeLevelTwoOp() || BlockUtil.canUseCompressedStonePickaxe(serverPlayer, world.getBlockState(pos));
  }

}
