package stone926.mods.more_enchantments.mixins;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.WetSpongeBlock;
import net.minecraft.state.StateManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import stone926.mods.more_enchantments.blocks.MyBlockStates;

@Mixin(WetSpongeBlock.class)
public class WetSpongeBlockMixin extends Block {

  public WetSpongeBlockMixin(Settings settings) {
    super(settings);
  }

  @Override
  protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
    super.appendProperties(builder);
    builder.add(MyBlockStates.POUR);
  }

  @Inject(method = "<init>", at = @At("RETURN"))
  private void init(Settings settings, CallbackInfo ci) {
    setDefaultState(stateManager.getDefaultState().with(MyBlockStates.POUR, false));
  }

  @Inject(method = "onBlockAdded", at = @At("HEAD"), cancellable = true)
  private void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean notify, CallbackInfo ci) {
    if (state.get(MyBlockStates.POUR)) {
      ci.cancel();
    }
  }

}
