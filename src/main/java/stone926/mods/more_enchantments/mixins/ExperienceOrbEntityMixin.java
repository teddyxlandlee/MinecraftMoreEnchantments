package stone926.mods.more_enchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Map;

import static stone926.mods.more_enchantments.MoreEnchantmentsMod.TEARING;

@Mixin(ExperienceOrbEntity.class)
public abstract class ExperienceOrbEntityMixin extends Entity {

  public ExperienceOrbEntityMixin(EntityType<?> type, World world) {
    super(type, world);
  }

  @Inject(method = "repairPlayerGears", at = @At("HEAD"), cancellable = true)
  private void repairPlayerGears1(PlayerEntity player, int amount, CallbackInfoReturnable<Integer> cir) {
    Map.Entry<EquipmentSlot, ItemStack> entry = EnchantmentHelper.chooseEquipmentWith(TEARING, player, (a) -> true);
    if (entry != null) {
      ItemStack stack = entry.getValue();
      stack.damage(amount, (ServerPlayerEntity) player, (p) -> p.sendToolBreakStatus(player.getActiveHand()));
      cir.setReturnValue(0);
    }
  }

}
