package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.entity.mob.ZombieVillagerEntity;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.interfaces.IEntitySpawnedByFlower;
import stone926.mods.more_enchantments.interfaces.IMobEntity;

@Mixin(ZombieEntity.class)
public class ZombieEntityMixin extends HostileEntity {

  protected ZombieEntityMixin(EntityType<? extends HostileEntity> entityType, World world) {
    super(entityType, world);
  }

  @Inject(method = "burnsInDaylight", at = @At("HEAD"), cancellable = true)
  protected void burnsInDaylight(CallbackInfoReturnable<Boolean> cir) {
    if (((IEntitySpawnedByFlower) this).avoidBurning()) cir.setReturnValue(false);
  }

  @Redirect(method = "onKilledOther", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/passive/VillagerEntity;convertTo(Lnet/minecraft/entity/EntityType;Z)Lnet/minecraft/entity/mob/MobEntity;"))
  private MobEntity convertTo(VillagerEntity villager, EntityType<ZombieVillagerEntity> targetType, boolean keepEquipment) {
    ZombieVillagerEntity zv;
    if (((IEntitySpawnedByFlower) villager).isSpawnedByMandala()) {
      zv = villager.convertTo(targetType, true);
      if (zv != null) {
        ((IEntitySpawnedByFlower) zv).setSpawnedByMandala(true);
        ((IMobEntity) zv).setAiDisableTicks(((IMobEntity) this).getAiDisableTicks());
      }
    } else zv = villager.convertTo(targetType, keepEquipment);
    return zv;
  }

}
