package stone926.mods.more_enchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.DragonFireballEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.enchantments.BlazeEnchantment;
import stone926.mods.more_enchantments.enchantments.EnderDragonEnchantment;
import stone926.mods.more_enchantments.interfaces.IPlayerEntityBlaze;
import stone926.mods.more_enchantments.util.RandomUtil;

@Mixin(Item.class)
public abstract class BlazeRodDragonBreathItemMixin implements ItemConvertible {

  @Inject(method = "use", at = @At("HEAD"), cancellable = true)
  private void use(World world, PlayerEntity user, Hand hand, CallbackInfoReturnable<TypedActionResult<ItemStack>> cir) {
    ItemStack stack = user.getStackInHand(hand);
    if (world.isClient) return;
    if (stack.isOf(Items.BLAZE_ROD)) {

      int blazeLvl = EnchantmentHelper.getLevel(MoreEnchantmentsMod.BLAZE, stack);
      int enhancedLvl = EnchantmentHelper.getLevel(MoreEnchantmentsMod.ENHANCED_SMALL_FIREBALL, stack);
      if (blazeLvl > 0 && user.experienceLevel != 0) {
        ((IPlayerEntityBlaze) user).shoot(BlazeEnchantment.getSmallFireballCount(blazeLvl, user.getRandom()) + enhancedLvl, 1);
        ((IPlayerEntityBlaze) user).boost(Math.max(enhancedLvl, 0));
        RandomUtil.runIfPossible(BlazeEnchantment.getStackDecrementChance(blazeLvl), user.getRandom(), () -> stack.decrement(1));
        user.addExperience(-blazeLvl * 20);
        BlazeEnchantment.damageUserRandomly(user, blazeLvl);
        cir.setReturnValue(TypedActionResult.success(stack));
      }

    } else if (stack.isOf(Items.DRAGON_BREATH)) {

      int enderDragonLvl = EnchantmentHelper.getLevel(MoreEnchantmentsMod.ENDER_DRAGON, stack);
      if (enderDragonLvl > 0 && user.experienceLevel != 0) {
        float f1 = -MathHelper.sin(user.getYaw() * 0.017453292F) * MathHelper.cos(user.getPitch() * 0.017453292F);
        float f2 = -MathHelper.sin((user.getPitch() + user.getRoll()) * 0.017453292F);
        float f3 = MathHelper.cos(user.getYaw() * 0.017453292F) * MathHelper.cos(user.getPitch() * 0.017453292F);

        DragonFireballEntity fireball = new DragonFireballEntity(world, user, f1, f2, f3);
        fireball.setPosition(user.getPos().add(0, user.getHeight(), 0));
        fireball.setOwner(user);
        fireball.setVelocity(user, user.getPitch(), user.getYaw(), 0, 1F + enderDragonLvl / 1.5F, enderDragonLvl * 3);
        world.spawnEntity(fireball);

        stack.decrement(1);
        user.addExperience(-enderDragonLvl * 20);
        EnderDragonEnchantment.damageUserRandomly(user, enderDragonLvl);
        cir.setReturnValue(TypedActionResult.success(stack));
      }

    }
  }

}
