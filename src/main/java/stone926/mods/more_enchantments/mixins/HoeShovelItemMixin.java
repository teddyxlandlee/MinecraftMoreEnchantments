package stone926.mods.more_enchantments.mixins;

import net.minecraft.item.HoeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.ShovelItem;
import net.minecraft.util.ActionResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import static net.minecraft.enchantment.EnchantmentHelper.getLevel;
import static stone926.mods.more_enchantments.MoreEnchantmentsMod.*;

@Mixin({ShovelItem.class, HoeItem.class})
public class HoeShovelItemMixin {

  @Inject(method = "useOnBlock", at = @At("HEAD"), cancellable = true)
  private void useOnBlock(ItemUsageContext context, CallbackInfoReturnable<ActionResult> cir) {
    ItemStack stack = context.getStack();
    if (hasCleanUpEnchantment(stack) || hasCreativeOnlyEnchantment(stack)) {
      cir.setReturnValue(ActionResult.PASS);
    }
  }

  private boolean hasCleanUpEnchantment(ItemStack stack) {
    return getLevel(DESTROY_CURSE, stack) > 1
      || getLevel(CULTIVATION, stack) > 0
      || getLevel(FIRE_EXTINGUISHER, stack) > 0
      || getLevel(MOWER, stack) > 0
      || getLevel(TRIMMER, stack) > 0;
  }

  private boolean hasCreativeOnlyEnchantment(ItemStack stack) {
    return getLevel(ULTIMATE_WEAPON, stack) > 1;
  }

}
