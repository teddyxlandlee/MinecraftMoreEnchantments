package stone926.mods.more_enchantments.mixins.accessors;

import net.minecraft.entity.mob.ZombieVillagerEntity;
import net.minecraft.server.world.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(ZombieVillagerEntity.class)
public interface ZombieVillagerAccessor {

  @Invoker
  void invokeFinishConversion(ServerWorld world);

}
