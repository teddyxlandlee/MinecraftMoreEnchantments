package stone926.mods.more_enchantments.mixins.accessors;

import net.minecraft.entity.mob.MobEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(MobEntity.class)
public interface MobEntityAccessor {

  @Accessor
  int getExperiencePoints();

  @Accessor
  void setExperiencePoints(int experiencePoints);

}
