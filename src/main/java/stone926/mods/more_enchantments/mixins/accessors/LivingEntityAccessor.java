package stone926.mods.more_enchantments.mixins.accessors;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(LivingEntity.class)
public interface LivingEntityAccessor {

  @Accessor("syncedArmorStacks")
  DefaultedList<ItemStack> getSyncedArmorStacks();

}
