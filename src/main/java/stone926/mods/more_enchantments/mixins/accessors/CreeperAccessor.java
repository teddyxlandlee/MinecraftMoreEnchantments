package stone926.mods.more_enchantments.mixins.accessors;

import net.minecraft.entity.mob.CreeperEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(CreeperEntity.class)
public interface CreeperAccessor {

  @Accessor
  int getFuseTime();

  @Accessor
  void setFuseTime(int fuseTime);

  @Accessor
  int getExplosionRadius();

  @Accessor
  void setExplosionRadius(int explosionRadius);

}
