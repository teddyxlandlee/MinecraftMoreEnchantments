package stone926.mods.more_enchantments.mixins.accessors;

import net.minecraft.entity.mob.MagmaCubeEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(MagmaCubeEntity.class)
public interface MagmaCubeEntityAccessor {

  @Invoker("setSize")
  void invokeSetSize(int size, boolean heal);

}
