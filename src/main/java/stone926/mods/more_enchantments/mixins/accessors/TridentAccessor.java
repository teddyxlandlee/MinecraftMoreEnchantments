package stone926.mods.more_enchantments.mixins.accessors;

import net.minecraft.entity.projectile.TridentEntity;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(TridentEntity.class)
public interface TridentAccessor {

  @Invoker
  ItemStack invokeAsItemStack();

  @Accessor
  void setTridentStack(ItemStack tridentStack);

}
