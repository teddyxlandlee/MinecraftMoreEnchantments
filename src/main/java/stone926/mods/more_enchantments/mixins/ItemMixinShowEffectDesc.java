package stone926.mods.more_enchantments.mixins;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionUtil;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

import java.util.List;

@Mixin(Item.class)
public abstract class ItemMixinShowEffectDesc implements ItemConvertible {

  private static final Text NONE_TEXT = new TranslatableText("effect.none").formatted(Formatting.GRAY);

  @Inject(method = "appendTooltip", at = @At("RETURN"))
  private void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context, CallbackInfo ci) {
    if (EnchantmentUtil.hasEnchantment(MoreEnchantmentsMod.SWORD_WITH_EFFECT, stack))
      PotionUtil.buildTooltip(stack, tooltip, 1.0F);
  }

}
