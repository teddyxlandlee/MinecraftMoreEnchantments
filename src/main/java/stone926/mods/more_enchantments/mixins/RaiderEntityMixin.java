package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.mob.PatrolEntity;
import net.minecraft.entity.mob.WitchEntity;
import net.minecraft.entity.raid.RaiderEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import stone926.mods.more_enchantments.interfaces.IWitch;
import stone926.mods.more_enchantments.nbt.CustomNbtKeys;

@Mixin(RaiderEntity.class)
public class RaiderEntityMixin extends PatrolEntity {

  protected RaiderEntityMixin(EntityType<? extends PatrolEntity> entityType, World world) {
    super(entityType, world);
  }

  @Inject(method = "readCustomDataFromNbt", at = @At("HEAD"))
  private void readCustomDataFromNbt(NbtCompound nbt, CallbackInfo ci) {
    if (((RaiderEntity) (Object) this) instanceof WitchEntity witch) {
      if (nbt.contains(CustomNbtKeys.WITCH.STRENGTHENED))
        ((IWitch) witch).setStrengthened(nbt.getBoolean(CustomNbtKeys.WITCH.STRENGTHENED));
    }
  }

  @Inject(method = "writeCustomDataToNbt", at = @At("HEAD"))
  private void writeCustomDataToNbt(NbtCompound nbt, CallbackInfo ci) {
    if (((RaiderEntity) (Object) this) instanceof WitchEntity witch) {
      nbt.putBoolean(CustomNbtKeys.WITCH.STRENGTHENED, ((IWitch) witch).isStrengthened());
    }
  }

}
