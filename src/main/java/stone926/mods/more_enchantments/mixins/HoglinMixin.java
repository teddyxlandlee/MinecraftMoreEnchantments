package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.mob.HoglinEntity;
import net.minecraft.entity.mob.ZoglinEntity;
import org.spongepowered.asm.mixin.Mixin;

@Mixin({HoglinEntity.class, ZoglinEntity.class})
public class HoglinMixin {

//  @Redirect(method = "tryAttack", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/mob/Hoglin;tryAttack(Lnet/minecraft/entity/LivingEntity;Lnet/minecraft/entity/LivingEntity;)Z"))
//  private boolean tryAttack(LivingEntity attacker, LivingEntity target) {
//    int lvl = EnchantmentHelper.getEquipmentLevel(MoreEnchantmentsMod.STABLE, target);
//    if (lvl > 0) {
//      return InterfaceCopy.tryAttack(attacker, target, StableEnchantment.getHoglinKnockbackChance(lvl));
//    } else {
//      return Hoglin.tryAttack(attacker, target);
//    }
//  }
//
//  @Redirect(method = "knockback", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/mob/Hoglin;knockback(Lnet/minecraft/entity/LivingEntity;Lnet/minecraft/entity/LivingEntity;)V"))
//  private void knockback(LivingEntity attacker, LivingEntity target) {
//    int lvl = EnchantmentHelper.getEquipmentLevel(MoreEnchantmentsMod.STABLE, target);
//    if (lvl > 0) {
//      RandomUtil.runIfPossible(StableEnchantment.getHoglinKnockbackChance(lvl), attacker.getRandom(), () -> Hoglin.knockback(attacker, target));
//    } else {
//      Hoglin.knockback(attacker, target);
//    }
//  }


}
