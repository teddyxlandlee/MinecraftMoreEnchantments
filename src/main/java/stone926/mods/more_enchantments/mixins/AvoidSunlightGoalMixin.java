package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.ai.goal.AvoidSunlightGoal;
import net.minecraft.entity.mob.PathAwareEntity;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.interfaces.IEntitySpawnedByFlower;

@Mixin(AvoidSunlightGoal.class)
public class AvoidSunlightGoalMixin {

  @Shadow
  @Final
  private PathAwareEntity mob;

  @Inject(method = "canStart", at = @At("RETURN"), cancellable = true)
  private void canStart(CallbackInfoReturnable<Boolean> cir) {
    cir.setReturnValue(cir.getReturnValueZ() && !((IEntitySpawnedByFlower) mob).avoidBurning());
  }

}
