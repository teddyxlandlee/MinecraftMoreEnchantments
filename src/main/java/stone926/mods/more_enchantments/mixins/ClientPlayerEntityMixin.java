package stone926.mods.more_enchantments.mixins;

import com.mojang.authlib.GameProfile;
import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.enchantments.legendary.FateEnchantment;
import stone926.mods.more_enchantments.interfaces.IFateCooldown;
import stone926.mods.more_enchantments.util.DamageUtil;
import stone926.mods.more_enchantments.util.EnchantmentUtil;
import stone926.mods.more_enchantments.util.RandomUtil;

@Mixin(ClientPlayerEntity.class)
public class ClientPlayerEntityMixin extends AbstractClientPlayerEntity {

  public ClientPlayerEntityMixin(ClientWorld world, GameProfile profile) {
    super(world, profile);
  }

  @Redirect(method = "applyDamage", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/ClientPlayerEntity;setHealth(F)V"))
  private void fate(ClientPlayerEntity victim, float health, DamageSource source, float amount) {
    int fateCooldown = ((IFateCooldown) this).getFateCooldown();
    if (fateCooldown == 0 && health <= 0 && !DamageUtil.isDecapitation(source) && !source.isOutOfWorld()) {
      int i = EnchantmentHelper.getEquipmentLevel(MoreEnchantmentsMod.FATE, victim);
      if (i > 0) {
        ((IFateCooldown) this).setFateCooldown(FateEnchantment.getCooldownTicks(EnchantmentUtil.getTotalEquipmentLvl(MoreEnchantmentsMod.FATE, victim)));
        return;
      }
      victim.addStatusEffect(new StatusEffectInstance(StatusEffects.RESISTANCE, RandomUtil.nextInt(60, 120, getRandom()), 6));
      victim.addStatusEffect(new StatusEffectInstance(StatusEffects.STRENGTH, RandomUtil.nextInt(50, 100, getRandom()), RandomUtil
        .nextInt(1, 3, random)));
      victim.addStatusEffect(new StatusEffectInstance(StatusEffects.INVISIBILITY, RandomUtil.nextInt(200, 250, getRandom()), 1));
    }
    victim.setHealth(health);
  }

}
