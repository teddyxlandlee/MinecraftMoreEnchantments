package stone926.mods.more_enchantments.mixins;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.util.BlockUtil;

@Mixin(AbstractBlock.class)
public class AbstractBlockMixin {

  @Inject(method = "calcBlockBreakingDelta", at = @At("HEAD"), cancellable = true)
  private void calcBlockBreakingDelta(BlockState state, PlayerEntity player, BlockView world, BlockPos pos, CallbackInfoReturnable<Float> cir) {
    if (BlockUtil.canUseCompressedStonePickaxe(player, state)) {
      cir.setReturnValue(1F);
    }
  }

}
