package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.projectile.AbstractFireballEntity;
import net.minecraft.entity.projectile.SmallFireballEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import stone926.mods.more_enchantments.enchantments.EnhancedSmallFireballEnchantment;
import stone926.mods.more_enchantments.interfaces.ILivingEntity;
import stone926.mods.more_enchantments.interfaces.ISmallFireballEnhanced;
import stone926.mods.more_enchantments.util.RandomUtil;

@Mixin(SmallFireballEntity.class)
public class SmallFireballEntityMixin extends AbstractFireballEntity implements ISmallFireballEnhanced {

  private int boost = 0;

  public SmallFireballEntityMixin(EntityType<? extends AbstractFireballEntity> entityType, World world) {
    super(entityType, world);
  }

  @Redirect(method = "onEntityHit", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;damage(Lnet/minecraft/entity/damage/DamageSource;F)Z"))
  private boolean onEntityHitRedirectDamage(Entity entity, DamageSource source, float amount) {
    if (boost != 0 && entity instanceof LivingEntity livingEntity) {
      if (RandomUtil.isPossible(EnhancedSmallFireballEnchantment.getForceDamageProbability(boost), random)) {
        return ((ILivingEntity) livingEntity).forceDamage(source, amount + boost);
      }
    }
    return entity.damage(source, amount);
  }

  @Override
  public int getBoost() {
    return boost;
  }

  @Override
  public void setBoost(int boost) {
    this.boost = boost;
  }

}
