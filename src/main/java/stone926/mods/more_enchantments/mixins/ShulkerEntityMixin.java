package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.mob.ShulkerEntity;
import net.minecraft.nbt.NbtCompound;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.interfaces.ILivingEntity;
import stone926.mods.more_enchantments.interfaces.IShulkerEntity;

import static stone926.mods.more_enchantments.nbt.CustomNbtKeys.SHULKER_BULLET.LEVITATION_AMPLIFIER;

@Mixin(ShulkerEntity.class)
public class ShulkerEntityMixin implements IShulkerEntity {

  private int levitationAmplifier = 0;
  private boolean levitationAmplifierChanged = false;

  @Inject(method = "tryTeleport", at = @At("HEAD"), cancellable = true)
  private void tryTeleport(CallbackInfoReturnable<Boolean> cir) {
    if (((ILivingEntity) this).getTpSuppression() > 0) cir.setReturnValue(false);
  }

  @Override
  public int getLevitationAmplifier() {
    return levitationAmplifier;
  }

  @Override
  public void setLevitationAmplifier(int levitationAmplifier) {
    levitationAmplifierChanged = true;
    this.levitationAmplifier = levitationAmplifier;
  }

  @Inject(method = "readCustomDataFromNbt", at = @At("HEAD"))
  private void readCustomDataFromNbt(NbtCompound nbt, CallbackInfo ci) {
    if (nbt.contains(LEVITATION_AMPLIFIER)) setLevitationAmplifier(nbt.getInt(LEVITATION_AMPLIFIER));
  }

  @Override
  public boolean levitationAmplifierChanged() {
    return levitationAmplifierChanged;
  }

  @Inject(method = "writeCustomDataToNbt", at = @At("HEAD"))
  private void writeCustomDataToNbt(NbtCompound nbt, CallbackInfo ci) {
    if (levitationAmplifierChanged()) nbt.putInt(LEVITATION_AMPLIFIER, getLevitationAmplifier());
  }

}
