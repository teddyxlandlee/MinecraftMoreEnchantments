package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

@Mixin(ItemEntity.class)
public abstract class ItemEntityMixin {

  @Shadow
  public abstract ItemStack getStack();

  @Inject(method = "isFireImmune", at = @At("RETURN"), cancellable = true)
  private void fireImmune(CallbackInfoReturnable<Boolean> cir) {
    cir.setReturnValue(cir.getReturnValueZ() || EnchantmentUtil.hasEnchantment(MoreEnchantmentsMod.FIRE_IMMUNE, getStack()));
  }

}
