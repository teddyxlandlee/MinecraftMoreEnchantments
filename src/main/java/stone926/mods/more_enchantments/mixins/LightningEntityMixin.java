package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.LightningEntity;
import net.minecraft.nbt.NbtCompound;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import stone926.mods.more_enchantments.interfaces.ILightningEntity;
import stone926.mods.more_enchantments.nbt.CustomNbtKeys;

import java.util.UUID;

@Mixin(LightningEntity.class)
public class LightningEntityMixin implements ILightningEntity {

  private UUID owner = null;

  @Override
  public UUID getOwner() {
    return owner;
  }

  @Override
  public void setOwner(UUID owner) {
    this.owner = owner;
  }

  @Inject(method = "writeCustomDataToNbt", at = @At("HEAD"))
  private void writeCustomDataToNbt(NbtCompound nbt, CallbackInfo ci) {
    if (getOwner() != null) {
      nbt.putUuid(CustomNbtKeys.LIGHTNING.OWNER, getOwner());
    }
  }

  @Inject(method = "readCustomDataFromNbt", at = @At("HEAD"))
  private void readCustomDataFromNbt(NbtCompound nbt, CallbackInfo ci) {
    if (nbt.contains(CustomNbtKeys.LIGHTNING.OWNER)) setOwner(nbt.getUuid(CustomNbtKeys.LIGHTNING.OWNER));
  }

}
