package stone926.mods.more_enchantments.mixins.bucketable;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Bucketable;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.WaterCreatureEntity;
import net.minecraft.entity.passive.FishEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.util.InterfaceCopy;

@Mixin(FishEntity.class)
public abstract class FishEntityMixin extends WaterCreatureEntity implements Bucketable {

  public FishEntityMixin(EntityType<? extends WaterCreatureEntity> entityType, World world) {
    super(entityType, world);
  }

  @Inject(method = "interactMob", at = @At("HEAD"), cancellable = true)
  private void interactMob(PlayerEntity player, Hand hand, CallbackInfoReturnable<ActionResult> cir) {
    if (EnchantmentHelper.getLevel(MoreEnchantmentsMod.POUR, player.getStackInHand(hand)) > 0
      && world.getDimension().isUltrawarm()) {
      player.damage(DamageSource.mob(this), 5);
      cir.setReturnValue(InterfaceCopy.tryBucket(player, hand, this).orElse(super.interactMob(player, hand)));
    }
  }

}
