package stone926.mods.more_enchantments.mixins;

import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.boss.dragon.EnderDragonPart;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

@Mixin(EnderDragonEntity.class)
public class EnderDragonMixin {

  @Inject(method = "damagePart", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/boss/dragon/phase/PhaseManager;setPhase(Lnet/minecraft/entity/boss/dragon/phase/PhaseType;)V"))
  private void decapitateHead(EnderDragonPart part, DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
    if (source.getAttacker() instanceof PlayerEntity player) {
      if (EnchantmentUtil.hasEquipmentEnchantment(MoreEnchantmentsMod.DECAPITATION, player)) {
        ItemEntity itemEntity = new ItemEntity(player.getEntityWorld(), player.getX(), player.getY(), player.getZ(), new ItemStack(Items.DRAGON_HEAD));
        itemEntity.setPickupDelay(0);
        player.getEntityWorld().spawnEntity(itemEntity);
      }
    }
  }

}
