package stone926.mods.more_enchantments.mixins;

import net.minecraft.block.Blocks;
import net.minecraft.item.*;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.blocks.MyBlockStates;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

@Mixin(BlockItem.class)
public class BlockItemMixin extends Item {

  public BlockItemMixin(Settings settings) {
    super(settings);
  }

  @Inject(method = "place(Lnet/minecraft/item/ItemPlacementContext;)Lnet/minecraft/util/ActionResult;", at = @At(value = "RETURN", ordinal = 4))
  private void place(ItemPlacementContext context, CallbackInfoReturnable<ActionResult> cir) {
    ItemStack stack = context.getStack();
    World world = context.getWorld();
    if (stack.isOf(Items.WET_SPONGE) && EnchantmentUtil.hasEnchantment(MoreEnchantmentsMod.POUR, stack)) {
      BlockPos pos = context.getBlockPos();
      if (world.getBlockState(pos).isOf(Blocks.SPONGE)) {
        boolean b = world.setBlockState(pos, Blocks.WET_SPONGE.getDefaultState().with(MyBlockStates.POUR, true), 11);
      }
    }
  }

}
