package stone926.mods.more_enchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.HoneyBottleItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MilkBucketItem;
import net.minecraft.item.PotionItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.enchantments.FastDrinkEnchantment;

@Mixin({PotionItem.class, HoneyBottleItem.class, MilkBucketItem.class})
public class FastDrinkMixin {

  @Inject(method = "getMaxUseTime", at = @At("RETURN"), cancellable = true)
  private void getMaxUseTime(ItemStack stack, CallbackInfoReturnable<Integer> cir) {
    int lvl = EnchantmentHelper.getLevel(MoreEnchantmentsMod.FAST_DRINK, stack);
    if (lvl > 0) {
      cir.setReturnValue(FastDrinkEnchantment.getMaxUseTime(lvl, cir.getReturnValue()));
    }
  }

}
