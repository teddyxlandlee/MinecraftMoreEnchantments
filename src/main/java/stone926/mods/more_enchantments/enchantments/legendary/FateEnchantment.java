package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

import static stone926.mods.more_enchantments.util.EnchantmentUtil.ALL_ARMOR;

public class FateEnchantment extends AbstractLegendaryEnchantment {

  public FateEnchantment() {
    super(EnchantmentTarget.ARMOR_HEAD, ALL_ARMOR);
  }

  public static int getCooldownTicks(int totalLvl) {
    return totalLvl < 20 ? 35 * 20 / totalLvl : 35;
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.isOf(Items.ELYTRA);
  }

}
