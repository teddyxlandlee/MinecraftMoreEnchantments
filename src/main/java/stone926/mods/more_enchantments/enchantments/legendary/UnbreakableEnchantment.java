package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class UnbreakableEnchantment extends AbstractLegendaryEnchantment {

  public UnbreakableEnchantment() {
    super(EnchantmentTarget.BREAKABLE, EnchantmentUtil.ALL_SLOT);
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return true;
  }

}
