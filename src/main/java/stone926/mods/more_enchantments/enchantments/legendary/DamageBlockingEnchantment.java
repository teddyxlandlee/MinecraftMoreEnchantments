package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.util.EnchantmentUtil;
import stone926.mods.more_enchantments.util.RandomUtil;

import java.util.Random;

public class DamageBlockingEnchantment extends AbstractLegendaryEnchantment {

  public DamageBlockingEnchantment() {
    super(EnchantmentTarget.ARMOR, EnchantmentUtil.ALL_ARMOR);
  }

  public static double getBlockChance(int totalLvl) {
    return Math.min(0.95, totalLvl * 0.12);
  }

  public static int getBlockCooldown(int maxLvl, Random r) {
    int base = RandomUtil.nextInt(50, 80, r);
    return Math.max(5, base - (maxLvl - 1) * 10);
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.isOf(Items.ELYTRA);
  }

}
