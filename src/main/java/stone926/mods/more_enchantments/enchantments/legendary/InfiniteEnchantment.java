package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class InfiniteEnchantment extends AbstractLegendaryEnchantment {

  public InfiniteEnchantment() {
    super(EnchantmentTarget.BOW, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public int getMinPower(int level) {
    return 20;
  }

  public int getMaxPower(int level) {
    return 50;
  }

  /**
   * 一级：药箭无限
   * 二级：对弩生效
   *
   * @return maxLevel
   */
  public int getMaxLevel() {
    return 3;
  }

  @Override
  protected boolean canAccept(Enchantment other) {
    return super.canAccept(other) && !other.equals(Enchantments.INFINITY);
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.isOf(Items.CROSSBOW);
  }

}
