package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;

public class StrafingEnchantment extends AbstractLegendaryEnchantment {

  public StrafingEnchantment() {
    super(EnchantmentTarget.CROSSBOW, new EquipmentSlot[]{EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND});
  }

  public static int getShootCount(int lvl) {
    return lvl > 5 ? 38 + lvl * 2 : 1 + lvl * 6;
  }

  public static float getRange(int lvl) {
    return lvl > 5 ? 31 + lvl * 2 : 21 + lvl * 2;
  }

  @Override
  public int getMaxLevel() {
    return 6;
  }

  @Override
  public void onTargetDamaged(LivingEntity user, Entity target, int level) {
    super.onTargetDamaged(user, target, level);
    if (user instanceof PlayerEntity player) {
      player.addExhaustion(0.1F);
      player.addExperience(-1);
    }
  }

}
