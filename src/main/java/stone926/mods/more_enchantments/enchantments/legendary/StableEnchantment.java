package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.util.math.Vec3d;

import static stone926.mods.more_enchantments.util.EnchantmentUtil.ALL_ARMOR;

public class StableEnchantment extends AbstractLegendaryEnchantment {

  public StableEnchantment() {
    super(EnchantmentTarget.ARMOR_FEET, ALL_ARMOR);
  }

  public static double getExplosionKnockbackMultiplier(int lvl) {
    return Math.max(0, 1 - lvl * 0.4);
  }

  public static double getHoglinKnockbackChance(int lvl) {
    return 1 - Math.min(1, lvl * 0.35);
  }

  public static Vec3d getKnockback(double x, double y, double z, int lvl) {
    double m = Math.max(0, 1 - 0.35 * lvl);
    return new Vec3d(x * m, y * m, z * m);
  }

  @Override
  public int getMaxLevel() {
    return 3;
  }

  @Override
  public int getMinPower(int level) {
    return level * 12;
  }

}
