package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.damage.StoneEntityDamageSource;
import stone926.mods.more_enchantments.util.EnchantmentUtil;
import stone926.mods.more_enchantments.util.RandomUtil;

import static stone926.mods.more_enchantments.util.EnchantmentUtil.ALL_ARMOR;

public class PurgeEnchantment extends AbstractLegendaryEnchantment {

  public PurgeEnchantment() {
    super(EnchantmentTarget.ARMOR_CHEST, ALL_ARMOR);
  }

  public static double getDamageLeftMultiplier(int lvl) {
    if (lvl <= 0) return 1;
    return 1 - lvl * 0.1;
  }

  public static boolean shouldRemoveEffect(LivingEntity livingEntity, StatusEffectInstance effect) {
    boolean isNegativeEffect = effect.getEffectType().getCategory() == StatusEffectCategory.HARMFUL;
    int lvl = EnchantmentHelper.getEquipmentLevel(MoreEnchantmentsMod.PURGE, livingEntity);
    if (isNegativeEffect) {
      return lvl > 1 || (lvl > 0 && livingEntity.isPlayer() && ((PlayerEntity) livingEntity).isCreative());
    } else {
      return false;
    }
  }

  public static boolean shouldPutOffFire(LivingEntity livingEntity) {
    return EnchantmentUtil.hasEquipmentEnchantment(MoreEnchantmentsMod.PURGE, livingEntity);
  }

  public static boolean shouldPreventExplosionCreation(LivingEntity livingEntity) {
    int lvl = EnchantmentHelper.getEquipmentLevel(MoreEnchantmentsMod.PURGE, livingEntity);
    if (lvl <= 0) {
      return false;
    } else if (lvl == 1) {
      return RandomUtil.isPossible(0.6, livingEntity.getRandom());
    } else {
      return true;
    }
  }

  @Override
  public int getProtectionAmount(int level, DamageSource source) {
    if (source instanceof StoneEntityDamageSource s && s.isExplosionCreator()) return 30 + level * 5;
    else return 20 + level * 2;
  }

  @Override
  public int getMaxLevel() {
    return 2;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.isOf(Items.ELYTRA);
  }

}
