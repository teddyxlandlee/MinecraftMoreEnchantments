package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public class AbstractLegendaryEnchantment extends Enchantment {

  protected AbstractLegendaryEnchantment(EnchantmentTarget type, EquipmentSlot[] slotTypes) {
    super(Rarity.VERY_RARE, type, slotTypes);
  }

  @Override
  public Text getName(int level) {
    MutableText mutableText = new TranslatableText(this.getTranslationKey());
    mutableText.formatted(Formatting.GOLD);

    if (level != 1 || this.getMaxLevel() != 1) {
      mutableText.append(" ").append(new TranslatableText("enchantment.level." + level));
    }

    return mutableText;
  }

  @Override
  public boolean isTreasure() {
    return true;
  }

  @Override
  public boolean isAvailableForEnchantedBookOffer() {
    return false;
  }

  @Override
  public boolean isAvailableForRandomSelection() {
    return false;
  }

}
