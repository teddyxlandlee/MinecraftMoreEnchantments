package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;

public class ThumpEnchantment extends AbstractLegendaryEnchantment {

  public ThumpEnchantment() {
    super(EnchantmentTarget.WEAPON, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public static float getExtraDamagePercentage(int lvl) {
    return 0.126F + lvl * 0.042F;
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.getItem() instanceof AxeItem || stack.getItem() instanceof TridentItem;
  }

  @Override
  public float getAttackDamage(int level, EntityGroup group) {
    return 3;
  }

}
