package stone926.mods.more_enchantments.enchantments.legendary;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class RetaliationEnchantment extends AbstractLegendaryEnchantment {

  public RetaliationEnchantment() {
    super(EnchantmentTarget.ARMOR, EnchantmentUtil.ALL_ARMOR);
  }

  public static double getRetaliationChance(int lvl) {
    return lvl * 0.08;
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.isOf(Items.ELYTRA);
  }

}
