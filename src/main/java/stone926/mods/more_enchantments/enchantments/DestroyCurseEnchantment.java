package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ItemStack;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

import java.util.List;

public class DestroyCurseEnchantment extends Enchantment {

  public DestroyCurseEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.VANISHABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public static int getToolDamage(int lvl) {
    return lvl * 2;
  }

  public static int getItemKilledRadius(int lvl) {
    return lvl * 9;
  }

  public static boolean cleanUp(BlockPos userPos, World world, PlayerEntity user, int lvl, Hand hand) {
    if (lvl <= 1) return false;
    Box box = Box.from(user.getPos()).expand(DestroyCurseEnchantment.getItemKilledRadius(lvl));
    boolean isSuccess = false;
    List<ItemEntity> itemEntityList = world.getEntitiesByClass(ItemEntity.class, box, EntityPredicates.VALID_ENTITY);
    List<ExperienceOrbEntity> xpEntityList = world.getEntitiesByClass(ExperienceOrbEntity.class, box, EntityPredicates.VALID_ENTITY);
    for (ItemEntity ie : itemEntityList) {
      if (world instanceof ServerWorld server) {
        server.spawnParticles(ParticleTypes.SMOKE, ie.getX(), ie.getY() + ie.getHeight() + 1, ie.getZ(), 30, 0.1, 0.6, 0.1, 0);
      }
      ie.kill();
      if (!isSuccess) isSuccess = true;
    }
    for (ExperienceOrbEntity xp : xpEntityList) {
      if (world instanceof ServerWorld server) {
        server.spawnParticles(
          ParticleTypes.GLOW,
          xp.getX(), xp.getY() + xp.getHeight() + 1, xp.getZ(),
          30,
          0.1, 0.6, 0.1,
          0
        );
      }
      xp.kill();
      if (!isSuccess) isSuccess = true;
    }
    user
      .getStackInHand(hand)
      .damage(DestroyCurseEnchantment.getToolDamage(lvl), user, (p) -> p.sendToolBreakStatus(user.getActiveHand()));
    return isSuccess;
  }

  @Override
  public boolean isCursed() {
    return true;
  }

  @Override
  public int getMinPower(int level) {
    return 25 + level * 2;
  }

  @Override
  public int getMaxLevel() {
    return 2;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) && !(stack.getItem() instanceof ArmorItem);
  }

  @Override
  protected boolean canAccept(Enchantment other) {
    return super.canAccept(other) && other != MoreEnchantmentsMod.MULTIPLY_CURSE;
  }

}
