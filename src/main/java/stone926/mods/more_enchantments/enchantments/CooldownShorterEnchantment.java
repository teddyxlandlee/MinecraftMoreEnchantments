package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;

public class CooldownShorterEnchantment extends Enchantment {

  public CooldownShorterEnchantment() {
    super(Rarity.RARE, EnchantmentTarget.VANISHABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public static double getShorterCooldown(int lvl) {
    if (lvl <= 0) return 1;
    return Math.max(0, 0.8 - lvl * 0.1);
  }

  @Override
  public int getMinPower(int level) {
    return 5 + 10 * (level - 1);
  }

  @Override
  public int getMaxLevel() {
    return 4;
  }

  @Override
  public boolean isTreasure() {
    return true;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return true;
  }

}
