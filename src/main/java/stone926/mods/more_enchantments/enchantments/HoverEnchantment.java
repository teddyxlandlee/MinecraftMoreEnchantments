package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class HoverEnchantment extends Enchantment {

  public HoverEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.BREAKABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND});
  }

  public static double getVelocityMultiplier(int lvl) {
    return lvl * 0.45;
  }

  @Override
  public int getMaxLevel() {
    return 3;
  }

  public int getMinPower(int level) {
    return level * 20;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.FIREWORK_ROCKET);
  }

}
