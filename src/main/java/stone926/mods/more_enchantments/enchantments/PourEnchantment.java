package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.BucketItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class PourEnchantment extends Enchantment {

  public PourEnchantment() {
    super(Rarity.RARE, EnchantmentTarget.VANISHABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND});
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return (stack.getItem() instanceof BucketItem
      && !stack.isOf(Items.LAVA_BUCKET)
      && !stack.isOf(Items.POWDER_SNOW_BUCKET))
      || stack.isOf(Items.WET_SPONGE);
  }

}
