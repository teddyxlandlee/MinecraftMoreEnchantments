package stone926.mods.more_enchantments.enchantments.cleanUp;

import net.minecraft.block.BlockState;
import net.minecraft.item.*;
import net.minecraft.tag.BlockTags;

public class TrimmerEnchantment extends AbstractCleanUpEnchantment {

  @Override
  public boolean shouldCleanUp(BlockState state, int lvl) {
    return state.isIn(BlockTags.LEAVES);
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    Item i = stack.getItem();
    return i instanceof ShearsItem || i instanceof AxeItem || i instanceof HoeItem;
  }

}
