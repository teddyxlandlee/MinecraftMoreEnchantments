package stone926.mods.more_enchantments.enchantments.cleanUp;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.*;
import stone926.mods.more_enchantments.tags.MyBlockTags;

public class MowerEnchantment extends AbstractCleanUpEnchantment {

  @Override
  public boolean shouldCleanUp(BlockState state, int lvl) {
    if (state.isOf(Blocks.AIR)) return false;
    if (state.isOf(Blocks.WATER)) return false;
    boolean isGrassLikeBlock = isGrassLikeBlock(state);
    boolean isSeaPlant = lvl > 1 && isSeaPlant(state);
    return isGrassLikeBlock || isSeaPlant;
  }

  private boolean isSeaPlant(BlockState state) {
    return state.isIn(MyBlockTags.SEA_PLANT);
  }

  private boolean isGrassLikeBlock(BlockState state) {
    return state.isIn(MyBlockTags.GRASS_LIKE_PLANT);
  }

  @Override
  public int getMaxLevel() {
    return 2;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    Item i = stack.getItem();
    return i instanceof AxeItem
      || i instanceof ShearsItem
      || i instanceof HoeItem
      || i instanceof SwordItem;
  }


}
