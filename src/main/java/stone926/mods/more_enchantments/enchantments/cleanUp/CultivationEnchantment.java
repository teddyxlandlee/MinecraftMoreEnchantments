package stone926.mods.more_enchantments.enchantments.cleanUp;

import net.minecraft.block.BlockState;
import net.minecraft.item.HoeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import stone926.mods.more_enchantments.tags.MyBlockTags;

public class CultivationEnchantment extends AbstractCleanUpEnchantment {

  @Override
  public boolean shouldCleanUp(BlockState state, int lvl) {
    return state.isIn(MyBlockTags.CULTIVATION);
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    Item i = stack.getItem();
    return i instanceof HoeItem;
  }

}
