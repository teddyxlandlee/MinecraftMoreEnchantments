package stone926.mods.more_enchantments.enchantments.cleanUp;

import net.minecraft.block.BlockState;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * 除草 Mower
 * 一级：草、花、树苗、藤蔓类 breakGrassLikeBlock
 * 二级：海洋类：海带、海草、珊瑚 breakSeaPlant
 * 可附魔于：SwordItem、AxeItem、HoeItem、ShearsItem
 * <p>
 * 修剪 Trimmer
 * 树叶 breakLeaves
 * 可附魔于：AxeItem、HoeItem、ShearsItem
 * <p>
 * 耕作 Cultivation
 * 农作物、仙人掌、甘蔗、竹子、竹子苗 breakCrops
 * 可附魔于：HoeItem
 * <p>
 * 灭火 FireExtinguisher
 * 火 extinguishFire
 * 可附魔于：ToolItem
 */
public abstract class AbstractCleanUpEnchantment extends Enchantment {

  public AbstractCleanUpEnchantment() {
    super(Rarity.COMMON, EnchantmentTarget.BREAKABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public int getCleanUpRadius(int lvl) {
    return Math.min(30, lvl * 5);
  }

  public int getToolDamage(int lvl) {
    return lvl;
  }

  public void addParticle(BlockPos blockPos, World world) {
  }

  public boolean cleanUp(BlockPos userPos, World world, PlayerEntity user, boolean shouldDrop, int lvl, Hand hand) {
    if (lvl <= 0) return false;
    boolean isSuccess = false;
    int radius = getCleanUpRadius(lvl);
    int cnt = 0;
    for (BlockPos pos : BlockPos.iterate(userPos.add(-radius, -radius, -radius), userPos.add(radius, radius, radius))) {
      BlockState state = world.getBlockState(pos);
      if (shouldCleanUp(state, lvl)) {
        addParticle(pos, world);
        world.breakBlock(pos, shouldDrop, user);
        cnt++;
        if (!isSuccess) isSuccess = true;
      }
    }
    if (isSuccess) user.getStackInHand(hand).damage(cnt + getToolDamage(lvl), user, p -> p.sendToolBreakStatus(hand));
    return isSuccess;
  }

  @Override
  public int getMinPower(int level) {
    return 30;
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }


  public abstract boolean shouldCleanUp(BlockState state, int lvl);

}
