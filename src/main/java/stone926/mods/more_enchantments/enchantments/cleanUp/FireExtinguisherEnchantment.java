package stone926.mods.more_enchantments.enchantments.cleanUp;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.tag.BlockTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class FireExtinguisherEnchantment extends AbstractCleanUpEnchantment {

  @Override
  public boolean shouldCleanUp(BlockState state, int lvl) {
    return state.isIn(BlockTags.FIRE);
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.getItem() instanceof ToolItem;
  }

  @Override
  public int getCleanUpRadius(int lvl) {
    return Math.min(40, lvl * 7);
  }

  @Override
  public void addParticle(BlockPos blockPos, World world) {
    if (world instanceof ServerWorld server) {
      BlockState state = world.getBlockState(blockPos);
      double x = blockPos.getX() + 0.5;
      double y = blockPos.getY();
      double z = blockPos.getZ() + 0.5;
      if (state.isOf(Blocks.SOUL_FIRE)) spawnSoulFlame(x, y, z, server);
      else spawnLava(x, y, z, server);
    }
  }

  private void spawnSoulFlame(double x, double y, double z, ServerWorld server) {
    server.spawnParticles(ParticleTypes.SOUL_FIRE_FLAME, x, y, z, 10, 0.2, 0.3, 0.2, 0);
  }

  private void spawnLava(double x, double y, double z, ServerWorld server) {
    server.spawnParticles(ParticleTypes.LAVA, x, y, z, 3, 0.1, 0.2, 0.1, 0);
  }

}
