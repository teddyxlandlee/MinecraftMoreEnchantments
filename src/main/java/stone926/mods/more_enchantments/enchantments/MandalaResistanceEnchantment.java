package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class MandalaResistanceEnchantment extends Enchantment {

  public MandalaResistanceEnchantment() {
    super(Rarity.UNCOMMON, EnchantmentTarget.ARMOR, EnchantmentUtil.ALL_ARMOR);
  }

  public static double getNotDrawProbability(int lvl) {
    return Math.min(1D, lvl * 0.35D);
  }

  public static float getDecreasedDamage(float damage, int lvl) {
    if (lvl <= 0) return damage;
    return (1F - Math.min(1F, lvl * 0.25F)) * damage;
  }

  @Override
  public int getMaxLevel() {
    return 3;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.isOf(Items.ELYTRA);
  }

}
