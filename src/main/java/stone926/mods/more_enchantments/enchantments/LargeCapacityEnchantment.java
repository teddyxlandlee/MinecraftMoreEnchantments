package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class LargeCapacityEnchantment extends Enchantment {

  public LargeCapacityEnchantment() {
    super(Rarity.COMMON, EnchantmentTarget.WEARABLE, EnchantmentUtil.ALL_HAND);
  }

  public static double getDecrementChance(int lvl) {
    return 1 - Math.max(0, lvl * 0.1);
  }

  public static double getPotionDecreaseChance(int lvl) {
    return 1 - Math.max(0, lvl * 0.06);
  }

  @Override
  public int getMaxLevel() {
    return 10;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.MILK_BUCKET);
  }

}
