package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class SacrificeResistanceEnchantment extends Enchantment {

  public SacrificeResistanceEnchantment() {
    super(Rarity.RARE, EnchantmentTarget.BREAKABLE, EnchantmentUtil.ALL_SLOT);
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return true;
  }

}
