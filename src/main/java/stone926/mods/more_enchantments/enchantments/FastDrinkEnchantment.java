package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class FastDrinkEnchantment extends Enchantment {

  public FastDrinkEnchantment() {
    super(Rarity.COMMON, EnchantmentTarget.WEARABLE, EnchantmentUtil.ALL_HAND);
  }

  public static int getMaxUseTime(int lvl, int maxUseTime) {
    if (lvl <= 0) return maxUseTime;
    return (int) Math.max(20, Math.round(maxUseTime * 0.6));
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.MILK_BUCKET) || stack.isOf(Items.POTION) || stack.isOf(Items.HONEY_BOTTLE);
  }

}
