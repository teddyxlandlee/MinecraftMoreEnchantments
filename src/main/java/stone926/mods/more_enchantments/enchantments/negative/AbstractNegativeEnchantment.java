package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public abstract class AbstractNegativeEnchantment extends Enchantment {

  protected EquipmentSlot[] slots;

  protected AbstractNegativeEnchantment(EnchantmentTarget type, EquipmentSlot[] slotTypes) {
    super(Enchantment.Rarity.UNCOMMON, type, slotTypes);
    this.slots = slotTypes;
  }


  @Override
  public Text getName(int level) {
    MutableText mutableText = new TranslatableText(this.getTranslationKey());
    mutableText.formatted(Formatting.WHITE);

    if (level != 1 || this.getMaxLevel() != 1) {
      mutableText.append(" ").append(new TranslatableText("enchantment.level." + level));
    }

    return mutableText;
  }

}
