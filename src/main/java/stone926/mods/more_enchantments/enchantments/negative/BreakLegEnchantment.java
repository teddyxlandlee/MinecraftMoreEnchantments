package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;

public class BreakLegEnchantment extends AbstractNegativeEnchantment {

  public BreakLegEnchantment() {
    super(EnchantmentTarget.ARMOR_FEET, new EquipmentSlot[]{EquipmentSlot.FEET});
  }

  public static int getExtraFallDamage(int lvl) {
    return Math.max(lvl, 0);
  }

  public int getMinPower(int level) {
    return 30;
  }

  public int getMaxPower(int level) {
    return getMinPower(level) + 10 * level;
  }

  public int getMaxLevel() {
    return 4;
  }

}
