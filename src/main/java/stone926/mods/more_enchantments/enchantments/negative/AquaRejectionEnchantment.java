package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;

public class AquaRejectionEnchantment extends AbstractNegativeEnchantment {

  public AquaRejectionEnchantment() {
    super(EnchantmentTarget.ARMOR_HEAD, new EquipmentSlot[]{EquipmentSlot.HEAD});
  }

  public int getMinPower(int level) {
    return 1;
  }

  public int getMaxPower(int level) {
    return this.getMinPower(level) + 40;
  }

  public int getMaxLevel() {
    return 1;
  }

}
