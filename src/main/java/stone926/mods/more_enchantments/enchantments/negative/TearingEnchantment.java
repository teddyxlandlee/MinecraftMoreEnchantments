package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;

import static stone926.mods.more_enchantments.util.EnchantmentUtil.ALL_SLOT;

public class TearingEnchantment extends AbstractNegativeEnchantment {

  public TearingEnchantment() {
    super(EnchantmentTarget.BREAKABLE, ALL_SLOT);
  }

  public int getMinPower(int level) {
    return level * 25;
  }

  public int getMaxPower(int level) {
    return this.getMinPower(level) + 50;
  }

  public boolean isTreasure() {
    return true;
  }

  public int getMaxLevel() {
    return 1;
  }

}
