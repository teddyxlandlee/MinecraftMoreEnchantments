package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;

public class LightningRodEnchantment extends AbstractNegativeEnchantment {

  public LightningRodEnchantment() {
    super(EnchantmentTarget.TRIDENT, new EquipmentSlot[]{EquipmentSlot.OFFHAND, EquipmentSlot.MAINHAND});
  }

  public int getMinPower(int level) {
    return 25;
  }

  public int getMaxPower(int level) {
    return 50;
  }

  public int getMaxLevel() {
    return 1;
  }


}
