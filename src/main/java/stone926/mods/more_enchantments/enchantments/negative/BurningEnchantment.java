package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;

import static stone926.mods.more_enchantments.util.EnchantmentUtil.ALL_ARMOR;

public class BurningEnchantment extends AbstractNegativeEnchantment {

  public BurningEnchantment() {
    super(EnchantmentTarget.ARMOR, ALL_ARMOR);
  }

  public static int getExtraBurningSeconds(int lvl) {
    return lvl > 0 ? lvl + 1 : 0;
  }

  public int getMinPower(int level) {
    return 30;
  }

  public int getMaxPower(int level) {
    return getMinPower(level) + 10 * level;
  }

  public int getMaxLevel() {
    return 4;
  }

}
