package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.interfaces.IPersistentProjectileEntity;
import stone926.mods.more_enchantments.mixins.accessors.ArrowEntityAccessor;

import static stone926.mods.more_enchantments.util.EnchantmentUtil.ALL_ARMOR;

public class TheTargetOfAllEnchantment extends AbstractNegativeEnchantment {

  public TheTargetOfAllEnchantment() {
    super(EnchantmentTarget.ARMOR, ALL_ARMOR);
  }

  public static double getProjectileYVelocity(int lvl) {
    return -lvl / 1.2D;
  }

  public static int getSpawnRadius(int lvl) {
    return lvl;
  }

  public static void spawnArrowsNearEntity(ServerWorld server, LivingEntity victim, Entity owner, PersistentProjectileEntity baseProjectile, int lvl) {
    if (lvl <= 0) return;
    BlockPos entityPos = victim.getBlockPos().withY((int) victim.getY() + 7);
    int r = getSpawnRadius(lvl);
    Iterable<BlockPos> iterator = BlockPos.iterate(entityPos.add(r, 0, r), entityPos.add(-r, 0, -r));
    for (BlockPos pos : iterator) {
      PersistentProjectileEntity projectile = (PersistentProjectileEntity) baseProjectile.getType().create(server);
      if (projectile != null) {
        if (projectile instanceof ArrowEntity arrowProjectile && baseProjectile instanceof ArrowEntity) {
          for (StatusEffectInstance effect : ((ArrowEntityAccessor) baseProjectile).getPotion().getEffects()) {
            arrowProjectile.addEffect(new StatusEffectInstance(
              effect.getEffectType(),
              Math.max(effect.getDuration() / 3, 1),
              effect.getAmplifier(),
              effect.isAmbient(),
              effect.shouldShowParticles()
            ));
          }
        }
        projectile.setOnFireFor(baseProjectile.getFireTicks());
        projectile.setPos(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5);
        projectile.setVelocity(0, getProjectileYVelocity(lvl), 0);
        projectile.setOwner(owner);
        projectile.setDamage(getDamage(lvl) + baseProjectile.getDamage() / 2.2f);
        projectile.pickupType = PersistentProjectileEntity.PickupPermission.CREATIVE_ONLY;

        IPersistentProjectileEntity projectile1 = (IPersistentProjectileEntity) projectile;
        projectile1.setEntityDamage(victim.getUuid());
        projectile1.setFromEnch(true);
        projectile1.setTraction(((IPersistentProjectileEntity) baseProjectile).getTraction());
        projectile1.setDisappearCountdown(10);
        projectile1.setForceDamage(true);
        server.spawnEntity(projectile);
      }
    }
  }

  public static double getDamage(int lvl) {
    return lvl * 1.2d;
  }

  @Override
  protected boolean canAccept(Enchantment other) {
    return super.canAccept(other) && other != MoreEnchantmentsMod.TEN_THOUSAND_ARROWS_AT_SHOOTER && other != MoreEnchantmentsMod.TEN_THOUSAND_ARROWS_AT_TARGET;
  }

  public int getMinPower(int level) {
    return 30;
  }

  public int getMaxPower(int level) {
    return getMinPower(level) + 10 * level;
  }

  public int getMaxLevel() {
    return 4;
  }

}
