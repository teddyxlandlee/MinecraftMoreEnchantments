package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.world.explosion.Explosion;

import java.util.Random;

import static stone926.mods.more_enchantments.util.EnchantmentUtil.ALL_ARMOR;

public class ExplosionCreatorEnchantment extends AbstractNegativeEnchantment {

  public ExplosionCreatorEnchantment() {
    super(EnchantmentTarget.ARMOR, ALL_ARMOR);
  }

  public static int getExplosionPower(int lvl) {
    return lvl + new Random().nextInt(2);
  }

  public static Explosion.DestructionType getDestructionType(int lvl) {
    return Explosion.DestructionType.NONE;
  }

  public static int getTntCount(int lvl, Random r) {
    return switch (lvl) {
      case 0 -> 0;
      case 1 -> 1;
      case 2 -> r.nextInt(3) == 0 ? 2 : 1 + r.nextInt(2);
      case 3 -> r.nextInt(2) == 0 ? 2 : 1 + r.nextInt(2);
      case 4 -> r.nextInt(3) == 0 ? 3 : 2 + r.nextInt(3);
      default -> lvl - 2 + r.nextInt(lvl - 2);
    };
  }

  @Override
  public int getProtectionAmount(int level, DamageSource source) {
    if (source.isExplosive() && level > 0) return level + 1;
    return super.getProtectionAmount(level, source);
  }

  public int getMinPower(int level) {
    return 30;
  }

  public int getMaxPower(int level) {
    return getMinPower(level) + 10 * level;
  }

  public int getMaxLevel() {
    return 4;
  }

}
