package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ItemStack;

import java.util.Random;

import static stone926.mods.more_enchantments.util.EnchantmentUtil.ALL_SLOT;

public class FlimsyEnchantment extends AbstractNegativeEnchantment {

  public FlimsyEnchantment() {
    super(EnchantmentTarget.BREAKABLE, ALL_SLOT);
  }

  public static boolean shouldDamage(ItemStack item, int level, Random random) {
    if (level <= 0) return false;
    if (item.getItem() instanceof ArmorItem && random.nextFloat() < 0.6F) {
      return false;
    } else {
      return random.nextInt(level + 1) > 0;
    }
  }

  public int getMinPower(int level) {
    return 5 + (level - 1) * 8;
  }

  public int getMaxPower(int level) {
    return super.getMinPower(level) + 50;
  }

  public int getMaxLevel() {
    return 3;
  }

  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isDamageable() || super.isAcceptableItem(stack);
  }


}
