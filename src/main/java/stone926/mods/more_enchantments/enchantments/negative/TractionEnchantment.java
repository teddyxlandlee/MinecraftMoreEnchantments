package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;

public class TractionEnchantment extends AbstractNegativeEnchantment {

  public TractionEnchantment() {
    super(EnchantmentTarget.BOW, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public int getMinPower(int level) {
    return 12 + (level - 1) * 20;
  }

  public int getMaxPower(int level) {
    return this.getMinPower(level) + 25;
  }

  public int getMaxLevel() {
    return 2;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.getItem() instanceof TridentItem;
  }

}
