package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;

public class BetrayalEnchantment extends AbstractNegativeEnchantment {

  public BetrayalEnchantment() {
    super(EnchantmentTarget.TRIDENT, new EquipmentSlot[]{EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND});
  }

  public int getMinPower(int level) {
    return 30 + level;
  }

  public int getMaxPower(int level) {
    return 50;
  }

  public int getMaxLevel() {
    return 1;
  }

}
