package stone926.mods.more_enchantments.enchantments.negative;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;

public class SuffocationEnchantment extends AbstractNegativeEnchantment {

  public SuffocationEnchantment() {
    super(EnchantmentTarget.ARMOR_HEAD, new EquipmentSlot[]{EquipmentSlot.HEAD});
  }

  public int getMinPower(int level) {
    return 10 * level;
  }

  public int getMaxPower(int level) {
    return this.getMinPower(level) + 30;
  }

  public int getMaxLevel() {
    return 3;
  }

}
