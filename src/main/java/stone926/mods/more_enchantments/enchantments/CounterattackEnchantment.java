package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShieldItem;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class CounterattackEnchantment extends Enchantment {

  public CounterattackEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.ARMOR, EnchantmentUtil.ALL_HAND);
  }

  public static float getDamage(int lvl) {
    return lvl;
  }

  @Override
  public int getMinPower(int level) {
    return 30 + level;
  }

  @Override
  public int getMaxLevel() {
    return 3;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.getItem() instanceof ShieldItem;
  }

}
