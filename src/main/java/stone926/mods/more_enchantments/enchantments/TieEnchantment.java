package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class TieEnchantment extends Enchantment {

  public TieEnchantment() {
    super(Rarity.COMMON, EnchantmentTarget.VANISHABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.LEAD);
  }

}
