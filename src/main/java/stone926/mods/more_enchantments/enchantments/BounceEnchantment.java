package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShieldItem;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class BounceEnchantment extends Enchantment {

  public BounceEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.ARMOR, EnchantmentUtil.ALL_HAND);
  }

  public static double getKnockbackStrength(int lvl) {
    return switch (lvl) {
      case 1 -> 1;
      case 2 -> 2;
      case 3 -> 2.5;
      default -> lvl;
    };
  }

  public static double getYVelocity(int lvl) {
    return lvl * 0.16;
  }

  @Override
  public int getMinPower(int level) {
    return 30 + level;
  }

  @Override
  public int getMaxLevel() {
    return 3;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.getItem() instanceof ShieldItem;
  }

}
