package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.HungerManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;

public class StarveEnchantment extends Enchantment {

  public StarveEnchantment() {
    super(Rarity.VERY_RARE, EnchantmentTarget.VANISHABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public void onTargetDamaged(LivingEntity user, Entity target, int level) {
    if (target instanceof PlayerEntity p) {
      HungerManager hungerManager = p.getHungerManager();
      hungerManager.setFoodLevel(0);
      hungerManager.setSaturationLevel(0);
      hungerManager.setExhaustion(4);
    }
  }

  @Override
  public boolean isAvailableForRandomSelection() {
    return false;
  }

  @Override
  public boolean isAvailableForEnchantedBookOffer() {
    return false;
  }

  @Override
  public boolean isTreasure() {
    return true;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.getItem() instanceof AxeItem || stack.getItem() instanceof TridentItem;
  }

  @Override
  public float getAttackDamage(int level, EntityGroup group) {
    return super.getAttackDamage(level, group);
  }

}
