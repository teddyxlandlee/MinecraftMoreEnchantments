package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class FertilizerEnchantment extends Enchantment {

  public FertilizerEnchantment() {
    super(Rarity.UNCOMMON, EnchantmentTarget.BREAKABLE, new EquipmentSlot[]{EquipmentSlot.OFFHAND, EquipmentSlot.MAINHAND});
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.BONE_MEAL);
  }

}
