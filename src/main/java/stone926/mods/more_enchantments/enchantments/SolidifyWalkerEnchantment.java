package stone926.mods.more_enchantments.enchantments;

import net.minecraft.block.BlockState;
import net.minecraft.block.FluidBlock;
import net.minecraft.block.Material;
import net.minecraft.block.ShapeContext;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

public class SolidifyWalkerEnchantment extends Enchantment {

  public SolidifyWalkerEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.ARMOR_FEET, new EquipmentSlot[]{EquipmentSlot.FEET});
  }

  public static void solidifyLava(LivingEntity entity, World world, BlockPos blockPos, int level) {
    if (entity.isOnGround()) {
      BlockState blockState = MoreEnchantmentsMod.SOLIDIFIED_LAVA.getDefaultState();
      float f = (float) Math.min(16, 2 + level);
      BlockPos.Mutable mutable = new BlockPos.Mutable();
      for (BlockPos blockPos2 : BlockPos.iterate(blockPos.add(-f, -1.0D, -f), blockPos.add(f, -1.0D, f))) {
        if (blockPos2.isWithinDistance(entity.getPos(), f)) {
          mutable.set(blockPos2.getX(), blockPos2.getY() + 1, blockPos2.getZ());
          BlockState blockState2 = world.getBlockState(mutable);
          if (blockState2.isAir()) {
            BlockState blockState3 = world.getBlockState(blockPos2);
            if (blockState3.getMaterial() == Material.LAVA
              && blockState3.get(FluidBlock.LEVEL) == 0
              && blockState.canPlaceAt(world, blockPos2)
              && world.canPlace(blockState, blockPos2, ShapeContext.absent())
            ) {
              world.setBlockState(blockPos2, blockState);
              world.createAndScheduleBlockTick(
                blockPos2, MoreEnchantmentsMod.SOLIDIFIED_LAVA, MathHelper.nextInt(entity.getRandom(), 60, 120)
              );
            }
          }
        }
      }

    }
  }

  public int getMinPower(int level) {
    return level * 10;
  }

  public int getMaxPower(int level) {
    return this.getMinPower(level) + 15;
  }

  public boolean isTreasure() {
    return true;
  }

  public int getMaxLevel() {
    return 2;
  }

}
