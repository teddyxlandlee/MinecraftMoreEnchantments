package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;

import java.util.Random;

public class ExperienceLootingEnchantment extends Enchantment {

  public ExperienceLootingEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.WEAPON, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public static double getExperienceDropProbability(int lvl) {
    return lvl * 0.3;
  }

  public static int getExperienceDropAmount(int lvl,Random random) {
    return lvl + random.nextInt(lvl + 3);
  }

  public int getMinPower(int level) {
    return 15 + (level - 1) * 9;
  }

  public int getMaxPower(int level) {
    return super.getMinPower(level) + 50;
  }

  public int getMaxLevel() {
    return 3;
  }

  @Override
  public boolean isTreasure() {
    return false;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.getItem() instanceof AxeItem || stack.getItem() instanceof TridentItem || super.isAcceptableItem(stack);
  }

}
