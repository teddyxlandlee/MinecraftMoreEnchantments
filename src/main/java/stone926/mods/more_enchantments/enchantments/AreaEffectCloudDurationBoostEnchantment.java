package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;

public class AreaEffectCloudDurationBoostEnchantment extends Enchantment {

  public AreaEffectCloudDurationBoostEnchantment() {
    super(Rarity.RARE, EnchantmentTarget.ARMOR, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public static int getExtraDuration(int lvl) {
    return lvl > 0 ? lvl * 10 : 0;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.getItem() instanceof AxeItem;
  }

  @Override
  public boolean isTreasure() {
    return true;
  }

  @Override
  public int getMaxLevel() {
    return 3;
  }

  @Override
  public int getMinPower(int level) {
    return 10;
  }

}
