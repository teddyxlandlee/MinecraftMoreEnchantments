package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;

public class BombEnchantment extends Enchantment {

  public BombEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.ARMOR, new EquipmentSlot[]{EquipmentSlot.HEAD, EquipmentSlot.CHEST, EquipmentSlot.LEGS, EquipmentSlot.FEET});
  }

  public static int getExplosionPower(int lvl, int count) {
    return lvl + count;
  }

  @Override
  public int getMinPower(int level) {
    return 30;
  }

  @Override
  public int getMaxLevel() {
    return 2;
  }

}
