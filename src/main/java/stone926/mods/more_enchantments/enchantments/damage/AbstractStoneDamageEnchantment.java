package stone926.mods.more_enchantments.enchantments.damage;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;

public abstract class AbstractStoneDamageEnchantment extends Enchantment {

  public AbstractStoneDamageEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.WEAPON, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  @Override
  public int getMinPower(int level) {
    return 5 + 30 * (level - 1);
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

  @Override
  public boolean isTreasure() {
    return false;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.getItem() instanceof AxeItem || super.isAcceptableItem(stack);
  }

  public abstract float getExtraDamageAmountForEntity(Entity e, int lvl);

}
