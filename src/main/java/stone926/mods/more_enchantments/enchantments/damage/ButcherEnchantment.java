package stone926.mods.more_enchantments.enchantments.damage;

import net.minecraft.entity.Entity;
import stone926.mods.more_enchantments.util.EntityUtil;

public class ButcherEnchantment extends AbstractStoneDamageEnchantment {

  @Override
  public float getExtraDamageAmountForEntity(Entity e, int lvl) {
    if (EntityUtil.isAnimal(e)) return lvl + 2;
    return 0;
  }


}
