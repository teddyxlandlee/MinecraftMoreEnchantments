package stone926.mods.more_enchantments.enchantments.damage;

import net.minecraft.entity.Entity;
import stone926.mods.more_enchantments.util.EntityUtil;

public class FishermanEnchantment extends AbstractStoneDamageEnchantment {

  @Override
  public int getMaxLevel() {
    return 6;
  }

  @Override
  public float getExtraDamageAmountForEntity(Entity e, int lvl) {
    if (EntityUtil.isFish(e)) return lvl;
    if (EntityUtil.isGuardian(e)) {
      if (lvl >= 6) return 30;
      else return lvl + 3;
    }
    if (EntityUtil.isElderGuardian(e)) {
      if (lvl >= 6) return 27;
      else return lvl + 4;
    }
    return 0;
  }

}
