package stone926.mods.more_enchantments.enchantments.damage;

import net.minecraft.entity.Entity;
import stone926.mods.more_enchantments.util.EntityUtil;

public class VillageDefenderEnchantment extends AbstractStoneDamageEnchantment {


  @Override
  public float getExtraDamageAmountForEntity(Entity e, int lvl) {
    if (EntityUtil.isIllager(e)) return lvl * 2;
    if (EntityUtil.isRavager(e)) return lvl * 3;
    return 0;
  }

}
