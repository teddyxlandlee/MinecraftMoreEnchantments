package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.damage.StoneDamageSource;
import stone926.mods.more_enchantments.util.EnchantmentUtil;
import stone926.mods.more_enchantments.util.RandomUtil;

public class EnderDragonEnchantment extends Enchantment {

  public EnderDragonEnchantment() {
    super(Rarity.RARE, EnchantmentTarget.BREAKABLE, EnchantmentUtil.ALL_HAND);
  }

  public static void damageUserRandomly(PlayerEntity user, int lvl) {
    if (lvl <= 0) return;
    RandomUtil.runIfPossible(lvl + 2F, user.getRandom(), () -> {
      user.damage(
        StoneDamageSource.ENDER_DRAGON_ENCHANTMENT, lvl * 4
      );
    });
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.DRAGON_BREATH);
  }

}
