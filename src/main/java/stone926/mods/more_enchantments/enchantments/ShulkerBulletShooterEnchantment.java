package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

import java.util.Random;

public class ShulkerBulletShooterEnchantment extends Enchantment {

  public ShulkerBulletShooterEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.BOW, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public static int getShootCount(int lvl,Random random) {
    return 1 + (random.nextInt(30) < lvl ? 1 : 0);
  }

  public static double getEntitySearchRadius(int lvl) {
    return (lvl + 1) * 5;
  }

  public static double getEntitySearchRadiusForMob(int lvl) {
    if (lvl <= 0) lvl = 0;
    return Math.min(35, (lvl + 3) * 5);
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(MoreEnchantmentsMod.SHULKER_BOW);
  }

  public int getMinPower(int level) {
    return 1 + (level - 1) * 10;
  }

  public int getMaxLevel() {
    return 5;
  }

  @Override
  protected boolean canAccept(Enchantment other) {
    return super.canAccept(other) && other != MoreEnchantmentsMod.WITHER_SKULL_SHOOTER && other != MoreEnchantmentsMod.RANDOM_FIREBALL_SHOOTER;
  }

}
