package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class OnslaughtEnchantment extends Enchantment {

  public OnslaughtEnchantment() {
    super(Rarity.RARE, EnchantmentTarget.WEAPON, EnchantmentUtil.ALL_HAND);
  }

  public static double getAttackSpeedAddition(int lvl, int miningFatigueLvl) {
    if (lvl <= 0) return 0;
    else if (lvl <= 5) return lvl * Math.max(0.8, 1.2 - (miningFatigueLvl + 1) * 0.15);
    else return 1024;
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.getItem() instanceof AxeItem || stack.getItem() instanceof TridentItem;
  }

  @Override
  public boolean isTreasure() {
    return super.isTreasure();
  }

  @Override
  public boolean isAvailableForEnchantedBookOffer() {
    return false;
  }

  @Override
  public boolean isAvailableForRandomSelection() {
    return false;
  }

}
