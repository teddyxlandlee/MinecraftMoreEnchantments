package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class FireImmuneEnchantment extends Enchantment {

  public FireImmuneEnchantment() {
    super(Rarity.COMMON, EnchantmentTarget.VANISHABLE, EnchantmentUtil.ALL_SLOT);
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public int getMinPower(int level) {
    return 20;
  }

}
