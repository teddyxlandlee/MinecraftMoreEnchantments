package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class ReinforcedGunpowderEnchantment extends Enchantment {

  public ReinforcedGunpowderEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.BREAKABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND});
  }

  public static float getExtraDamage(int lvl) {
    return lvl;
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

  public int getMinPower(int level) {
    return level * 15;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.FIREWORK_ROCKET);
  }

}
