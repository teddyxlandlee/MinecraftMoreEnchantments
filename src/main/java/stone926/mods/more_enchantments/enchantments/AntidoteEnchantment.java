package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class AntidoteEnchantment extends Enchantment {

  public AntidoteEnchantment() {
    super(Rarity.COMMON, EnchantmentTarget.WEARABLE, EnchantmentUtil.ALL_HAND);
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  @Override
  public int getMinPower(int level) {
    return 20;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.MILK_BUCKET) /*|| stack.isOf(Items.ENCHANTED_GOLDEN_APPLE)*/;
  }

}
