package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import stone926.mods.more_enchantments.util.RandomUtil;

public class VampireEnchantment extends Enchantment {

  public VampireEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.WEAPON, new EquipmentSlot[]{ EquipmentSlot.MAINHAND });
  }

  private static float getHealHealth(int lvl) {
    return lvl / 2F;
  }

  public static double getHealProbability(int lvl) {
    return 0.14 * lvl;
  }

  @Override
  public int getMinPower(int level) {
    return 5 + 30 * (level - 1);
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

  @Override
  public boolean isTreasure() {
    return false;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.getItem() instanceof AxeItem || stack.getItem() instanceof TridentItem || super.isAcceptableItem(stack);
  }

  @Override
  public void onTargetDamaged(LivingEntity user, Entity target, int level) {
    super.onTargetDamaged(user, target, level);
    if (!user.isPlayer() && RandomUtil.isPossible(getHealProbability(level), user.getRandom())) heal(user, target, level);
  }

  public static void heal(LivingEntity user, Entity target, int lvl) {
    if (lvl > 0 && user.getEntityWorld() instanceof ServerWorld server) {
      user.heal(getHealHealth(lvl));
      server.spawnParticles(ParticleTypes.HEART, user.getX(), user.getY() + user.getHeight() + 1, user.getZ(), 7, 0.15, 0.4, 0.15, 0.2);
    }
  }

  @Override
  public float getAttackDamage(int level, EntityGroup group) {
    return -2.5F + level * 0.7F;
  }

}
