package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ItemStack;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

public class MultiplyCurseEnchantment extends Enchantment {

  public MultiplyCurseEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.VANISHABLE, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public static double getDropStackProbability(int lvl) {
    return lvl * 0.12;
  }

  public static double getArmorRemovedProbability(int lvl) {
    return lvl * 0.08;
  }

  public static float getPlayerDamageAmount(int lvl, PlayerEntity p) {
    return p.getMaxHealth() / 5F * lvl;
  }

  public static int getArmorDamageAmount(int lvl, PlayerEntity p) {
    return (int) (getPlayerDamageAmount(lvl, p) * 4);
  }

  @Override
  public boolean isCursed() {
    return true;
  }

  @Override
  public int getMinPower(int level) {
    return 25 + level * 2;
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) && !(stack.getItem() instanceof ArmorItem);
  }

  @Override
  protected boolean canAccept(Enchantment other) {
    return super.canAccept(other) && other != MoreEnchantmentsMod.DESTROY_CURSE;
  }

}
