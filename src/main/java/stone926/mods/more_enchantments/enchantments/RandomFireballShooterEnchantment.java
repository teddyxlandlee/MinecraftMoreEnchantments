package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.DragonFireballEntity;
import net.minecraft.entity.projectile.ExplosiveProjectileEntity;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.entity.projectile.SmallFireballEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

import java.util.Random;

public class RandomFireballShooterEnchantment extends Enchantment {

  public RandomFireballShooterEnchantment() {
    super(Enchantment.Rarity.COMMON, EnchantmentTarget.BOW, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public static float getExtraModifierZ(int lvl,Random random) {
    return lvl * 0.13F + (float) random.nextGaussian();
  }

  public static ExplosiveProjectileEntity getFireball(int lvl, LivingEntity user, World world, float pullProgress, int flashLvl) {
    float f1 = -MathHelper.sin(user.getYaw() * 0.017453292F) * MathHelper.cos(user.getPitch() * 0.017453292F);
    float f2 = -MathHelper.sin((user.getPitch() + user.getRoll()) * 0.017453292F);
    float f3 = MathHelper.cos(user.getYaw() * 0.017453292F) * MathHelper.cos(user.getPitch() * 0.017453292F);
    Random r = new Random();
    ExplosiveProjectileEntity fireball = switch (r.nextInt(13) /* [0,12] */ + r.nextInt((lvl > 0 ? 0 : (lvl + 2)) + 1)) {
      case 0, 1, 2, 3, 8 -> new DragonFireballEntity(world, user, f1, f2, f3);
      case 4, 5, 6, 7 -> new SmallFireballEntity(world, user, f1, f2, f3);
      default -> new FireballEntity(world, user, f1, f2, f3, getFireballPower(lvl));
    };
    float modifierZ = 2 + getExtraModifierZ(lvl,user.getRandom()) + flashLvl * 0.15F;
    fireball.setVelocity(
      user, user.getPitch(), user.getYaw(), 0.0F, modifierZ, -1 * pullProgress + 1
    );
    fireball.setPosition(
      user.getX(),
      user.getY() + user.getEyeHeight(user.getPose()) - fireball.getEyeHeight(fireball.getPose()),
      user.getZ()
    );
    fireball.setOwner(user);
    return fireball;
  }

  public static int getFireballPower(int lvl) {
    return lvl + 1;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(MoreEnchantmentsMod.FIREBALL_BOW);
  }

  public int getMinPower(int level) {
    return 1 + (level - 1) * 10;
  }

  public int getMaxLevel() {
    return 5;
  }

  @Override
  protected boolean canAccept(Enchantment other) {
    return super.canAccept(other) && other != MoreEnchantmentsMod.WITHER_SKULL_SHOOTER && other != MoreEnchantmentsMod.SHULKER_BULLET_SHOOTER;
  }

}
