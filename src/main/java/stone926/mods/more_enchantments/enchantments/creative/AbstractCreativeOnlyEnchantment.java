package stone926.mods.more_enchantments.enchantments.creative;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import stone926.mods.more_enchantments.util.EnchantmentUtil;

public class AbstractCreativeOnlyEnchantment extends Enchantment {

  public AbstractCreativeOnlyEnchantment() {
    super(Rarity.VERY_RARE, EnchantmentTarget.VANISHABLE, EnchantmentUtil.ALL_SLOT);
  }

  @Override
  public final boolean isAcceptableItem(ItemStack stack) {
    return true;
  }

  @Override
  protected final boolean canAccept(Enchantment other) {
    return true;
  }

  @Override
  public final boolean isTreasure() {
    return true;
  }

  @Override
  public final boolean isAvailableForRandomSelection() {
    return false;
  }

  @Override
  public final boolean isAvailableForEnchantedBookOffer() {
    return false;
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }

  public Text getName(int level) {
    MutableText mutableText = new TranslatableText(this.getTranslationKey());
    mutableText.formatted(Formatting.GREEN);

    if (level != 1 || this.getMaxLevel() != 1) {
      mutableText.append(" ").append(new TranslatableText("enchantment.level." + level));
    }

    return mutableText;
  }

}
