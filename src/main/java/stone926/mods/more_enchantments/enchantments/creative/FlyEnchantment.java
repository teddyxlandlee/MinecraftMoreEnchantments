package stone926.mods.more_enchantments.enchantments.creative;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.MathHelper;

public class FlyEnchantment extends AbstractCreativeOnlyEnchantment {

  public static void fly(PlayerEntity user, int lvl) {
    float f1 = -MathHelper.sin(user.getYaw() * 0.017453292F) * MathHelper.cos(user.getPitch() * 0.017453292F);
    float f2 = -MathHelper.sin((user.getPitch() + user.getRoll()) * 0.017453292F);
    float f3 = MathHelper.cos(user.getYaw() * 0.017453292F) * MathHelper.cos(user.getPitch() * 0.017453292F);
    user.addVelocity(f1 * (lvl + 0.5), f2 * (lvl + 1.3), f3 * (lvl + 0.5));
  }

}
