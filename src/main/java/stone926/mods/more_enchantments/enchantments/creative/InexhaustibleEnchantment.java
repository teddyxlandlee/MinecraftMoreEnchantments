package stone926.mods.more_enchantments.enchantments.creative;

public class InexhaustibleEnchantment extends AbstractCreativeOnlyEnchantment {

  public InexhaustibleEnchantment() {
    super();
  }

  @Override
  public int getMaxLevel() {
    return 1;
  }


}
