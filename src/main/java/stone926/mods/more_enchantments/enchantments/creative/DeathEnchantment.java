package stone926.mods.more_enchantments.enchantments.creative;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.boss.dragon.EnderDragonPart;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import stone926.mods.more_enchantments.damage.StoneEntityDamageSource;
import stone926.mods.more_enchantments.interfaces.ILivingEntity;

public class DeathEnchantment extends AbstractCreativeOnlyEnchantment {

  public static void kill(LivingEntity victim, PlayerEntity killer) {
    if (killer.getAbilities().creativeMode) {
      ((ILivingEntity) victim).forceDamage(StoneEntityDamageSource.death(killer), Integer.MAX_VALUE);
    }
  }

  public static void kill(EnderDragonPart part, PlayerEntity killer) {
    kill(part.owner, killer);
  }

  @Override
  public void onTargetDamaged(LivingEntity user, Entity target, int level) {
    if (user.getEntityWorld() instanceof ServerWorld && user instanceof PlayerEntity player) {
      if (target instanceof LivingEntity l)
        kill(l, player);
      else if (target instanceof EnderDragonPart p)
        kill(p, player);
    }
  }

}
