package stone926.mods.more_enchantments.enchantments.creative;

import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.text.*;
import net.minecraft.util.Formatting;
import net.minecraft.util.Util;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

public class UltimateWeaponEnchantment extends AbstractCreativeOnlyEnchantment {

  public static int getKillingRadius() {
    return 20;
  }

  public static void killEntities(Box box, World world, PlayerEntity user) {
    world.getEntitiesByClass(Entity.class, box, EntityPredicates.VALID_ENTITY).forEach((entity) -> {
      if (!(entity instanceof PlayerEntity)) {
        entity.remove(Entity.RemovalReason.DISCARDED);
        Text styledUserMsg = new TranslatableText("message.stone_more_enchantments.origin", user.getDisplayName()).formatted(Formatting.YELLOW);
        Text styledTargetMsg = new TranslatableText("message.stone_more_enchantments.origin", entity.getDisplayName()).formatted(Formatting.YELLOW);
        Text position = Texts
          .bracketed(new TranslatableText("chat.coordinates", (int) entity.getX(), (int) entity.getY(), (int) entity.getZ()))
          .formatted(Formatting.GREEN)
          .styled((style -> {
            return style
              .withClickEvent(new ClickEvent(
                ClickEvent.Action.SUGGEST_COMMAND,
                new StringBuilder()
                  .append("/tp @s").append(" ")
                  .append((int) entity.getX()).append(" ")
                  .append((int) entity.getY()).append(" ")
                  .append((int) entity.getZ())
                  .toString()
              ))
              .withHoverEvent(new HoverEvent(
                net.minecraft.text.HoverEvent.Action.SHOW_TEXT,
                new TranslatableText("message.stone_more_enchantments.click_to_teleport")
              ));
          }));
        if (user instanceof ClientPlayerEntity clientPlayerEntity)
          clientPlayerEntity.sendSystemMessage(new TranslatableText("message.stone_more_enchantments.ultimate_weapon_kill_entity", styledUserMsg, styledTargetMsg, position), Util.NIL_UUID);
      }
    });
  }

}
