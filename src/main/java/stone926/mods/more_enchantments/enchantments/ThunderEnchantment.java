package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;

/**
 * OnHit 就生成闪电
 */
public class ThunderEnchantment extends Enchantment {

  public ThunderEnchantment() {
    super(Rarity.VERY_RARE, EnchantmentTarget.TRIDENT, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
  }

  public int getMinPower(int level) {
    return 25;
  }

  public int getMaxPower(int level) {
    return 50;
  }

  public int getMaxLevel() {
    return 1;
  }

  @Override
  public boolean isAvailableForEnchantedBookOffer() {
    return false;
  }

  @Override
  public boolean isAvailableForRandomSelection() {
    return false;
  }

  @Override
  protected boolean canAccept(Enchantment other) {
    return super.canAccept(other) && !other.equals(Enchantments.CHANNELING);
  }

}
