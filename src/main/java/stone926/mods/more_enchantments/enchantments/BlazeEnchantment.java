package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.damage.StoneDamageSource;
import stone926.mods.more_enchantments.util.EnchantmentUtil;
import stone926.mods.more_enchantments.util.RandomUtil;

import java.util.Random;

public class BlazeEnchantment extends Enchantment {

  public BlazeEnchantment() {
    super(Rarity.UNCOMMON, EnchantmentTarget.BREAKABLE, EnchantmentUtil.ALL_HAND);
  }

  public static int getSmallFireballCount(int lvl, Random random) {
    int extra = 0;
    if (lvl > 1) extra = random.nextInt(lvl);
    return lvl + extra + 1;
  }

  public static double getStackDecrementChance(int lvl) {
    return 1 - lvl * 0.1;
  }

  public static void damageUserRandomly(PlayerEntity user, int lvl) {
    if (lvl <= 0) return;
    RandomUtil.runIfPossible(lvl + 4, user.getRandom(), () -> {
      user.damage(
        StoneDamageSource.BLAZE_ENCHANTMENT, lvl + 1
      );
    });
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.BLAZE_ROD);
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

}
