package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.TridentItem;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

public class FlashEnchantment extends Enchantment {

  public FlashEnchantment() {
    super(Rarity.UNCOMMON, EnchantmentTarget.CROSSBOW, new EquipmentSlot[]{EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND});
  }

  @Override
  public int getMaxLevel() {
    return 3;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return super.isAcceptableItem(stack) || stack.isOf(Items.BOW) || stack.getItem() instanceof TridentItem || stack.isOf(MoreEnchantmentsMod.WITHER_SKULL_BOW) || stack.isOf(MoreEnchantmentsMod.FIREBALL_BOW);
  }

}
