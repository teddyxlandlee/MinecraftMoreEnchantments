package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import stone926.mods.more_enchantments.damage.StoneDamageSource;
import stone926.mods.more_enchantments.util.EnchantmentUtil;
import stone926.mods.more_enchantments.util.RandomUtil;

import java.util.Random;

public class FireballThrowerEnchantment extends Enchantment {

  public FireballThrowerEnchantment() {
    super(Rarity.RARE, EnchantmentTarget.BREAKABLE, EnchantmentUtil.ALL_HAND);
  }

  public static int getExplosionPower(int lvl, Random random) {
    if (lvl == 1) return 2;
    return lvl + 1 + random.nextInt(lvl / 2);
  }

  public static void damageUserRandomly(PlayerEntity user, int lvl) {
    RandomUtil.runIfPossible(
      lvl * 0.08, user.getRandom(),
      () -> {user.damage(StoneDamageSource.FIREBALL_THROWER_ENCHANTMENT, lvl);}
    );
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.FIRE_CHARGE);
  }

  @Override
  public int getMaxLevel() {
    return 8;
  }

}
