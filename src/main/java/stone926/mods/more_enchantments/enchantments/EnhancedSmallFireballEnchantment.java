package stone926.mods.more_enchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class EnhancedSmallFireballEnchantment extends Enchantment {

  public EnhancedSmallFireballEnchantment() {
    super(Rarity.VERY_RARE, EnchantmentTarget.BREAKABLE, new EquipmentSlot[]{
      EquipmentSlot.OFFHAND, EquipmentSlot.MAINHAND});
  }

  public static double getForceDamageProbability(int lvl) {
    return lvl * 0.2;
  }

  public static float getModifierXYZDecrement(int lvl) {
    return lvl * 0.6F;
  }

  public boolean isAcceptableItem(ItemStack stack) {
    return stack.isOf(Items.BLAZE_ROD);
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

}
