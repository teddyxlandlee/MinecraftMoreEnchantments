package stone926.mods.more_enchantments.tags;

import net.minecraft.block.Block;
import net.minecraft.tag.TagKey;
import net.minecraft.util.registry.Registry;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;

public class MyBlockTags {

  public static final TagKey<Block> GOLDEN_ANVIL = tag("golden_anvil");
  public static final TagKey<Block> COMPRESSED_STONE_PICKAXE_MINEABLE = tag("compressed_stone_pickaxe_mineable");
  public static final TagKey<Block> CULTIVATION = tag("clean_up/cultivation");
  public static final TagKey<Block> COMMAND_BLOCK = tag("command_block");
  public static final TagKey<Block> GRASS_LIKE_PLANT = tag("clean_up/grass_like_plant");
  public static final TagKey<Block> SEA_PLANT = tag("clean_up/sea_plant");

  private static TagKey<Block> tag(String id) {
    return TagKey.of(Registry.BLOCK_KEY,MoreEnchantmentsMod.id(id));
  }

}
