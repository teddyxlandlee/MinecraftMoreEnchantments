package stone926.mods.more_enchantments.interfaces;

import net.minecraft.entity.damage.DamageSource;

import java.util.UUID;

public interface ILivingEntity {

  boolean forceDamage(DamageSource source, float amount);

  void setDropsNothing(boolean dropsNothing);

  boolean dropsNothing();

  int getHealedByCornelCooldown();

  void setHealedByCornelCooldown(int healByCornelCooldown);

  boolean canHealedByCornel();

  int getTpSuppression();

  void setTpSuppression(int ticks);

  UUID getVampire();

  void setVampire(UUID uuid);

  void removeVampire();

}
