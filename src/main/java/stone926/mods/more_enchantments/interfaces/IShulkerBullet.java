package stone926.mods.more_enchantments.interfaces;

public interface IShulkerBullet {

  float getExtraBulletDamage();

  void setExtraBulletDamage(float damage);

  int getFlash();

  void setFlash(int flash);

}
