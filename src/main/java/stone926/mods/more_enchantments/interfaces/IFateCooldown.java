package stone926.mods.more_enchantments.interfaces;

public interface IFateCooldown {

  int getFateCooldown();

  void setFateCooldown(int fateCooldown);

}
