package stone926.mods.more_enchantments.interfaces;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particle.ParticleEffect;

public interface IEntityScheduleDie {

  void scheduleToDie(int ticks);

  int getTicksFromDie();

  void setNeedDie(boolean needDie);

  boolean needDie();

  PlayerEntity getAttacker();

  void setAttacker(PlayerEntity player);

  int getLvl();

  void setLvl(int lvl);

  <U extends ParticleEffect> void spawnParticleBeforeDeath(
    U particle,
    double x, double y, double z,
    int count,
    double deltaX, double deltaY, double deltaZ,
    double speed
  );

  <T extends ParticleEffect> void setParticle(
    T particle,
    int count,
    double deltaX, double deltaY, double deltaZ,
    double speed
  );

}
