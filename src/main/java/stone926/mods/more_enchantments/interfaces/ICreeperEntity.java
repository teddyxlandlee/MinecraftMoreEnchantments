package stone926.mods.more_enchantments.interfaces;

public interface ICreeperEntity {

  boolean destructionTypeNone();

  void setDestructionTypeNone(boolean destructionTypeModified);

}
