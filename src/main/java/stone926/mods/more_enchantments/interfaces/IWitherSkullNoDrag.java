package stone926.mods.more_enchantments.interfaces;

public interface IWitherSkullNoDrag {

  float getDrag();

  void setDrag(float drag);

  boolean dragModified();

}
