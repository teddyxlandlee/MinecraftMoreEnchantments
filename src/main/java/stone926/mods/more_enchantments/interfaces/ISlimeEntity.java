package stone926.mods.more_enchantments.interfaces;

public interface ISlimeEntity {

  boolean shouldLowJump();

  void setLowJump(boolean lowJump);

  int getPreventSplitSize();

  void setPreventSplitSize(int size);

  int getAttackSpeed();

  void setAttackSpeed(int attackSpeed);

}
