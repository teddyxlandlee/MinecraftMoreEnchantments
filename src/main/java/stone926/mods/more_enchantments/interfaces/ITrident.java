package stone926.mods.more_enchantments.interfaces;

public interface ITrident {

  float getExtraDamage();

  void setExtraDamage(float damage);

}
