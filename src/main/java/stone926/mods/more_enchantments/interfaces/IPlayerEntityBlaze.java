package stone926.mods.more_enchantments.interfaces;

public interface IPlayerEntityBlaze {

  void shoot(int shootCount, int shootCooldown);

  void boost(int lvl);

}
