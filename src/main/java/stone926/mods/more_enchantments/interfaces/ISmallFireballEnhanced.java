package stone926.mods.more_enchantments.interfaces;

public interface ISmallFireballEnhanced {

  int getBoost();

  void setBoost(int boost);

}
