package stone926.mods.more_enchantments.interfaces;

public interface IMobEntity {

  int getTie();

  void setTie(int tie);

  int getAiDisableTicks();

  void setAiDisableTicks(int tick);

}
