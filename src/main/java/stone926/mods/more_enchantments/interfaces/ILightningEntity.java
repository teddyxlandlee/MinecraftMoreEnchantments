package stone926.mods.more_enchantments.interfaces;


import java.util.UUID;

public interface ILightningEntity {

  UUID getOwner();

  void setOwner(UUID owner);

}
