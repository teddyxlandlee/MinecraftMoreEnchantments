package stone926.mods.more_enchantments.interfaces;

public interface IRocketReinforced {

  double getExtraDamage();

  void setExtraDamage(double extraDamage);

}
