package stone926.mods.more_enchantments.interfaces;

public interface IWitherSkull {

  void setDrag(float drag);

  float getModifiedDrag();

  boolean dragModified();

  float getExtraDamage();

  void setExtraDamage(float extraDamage);

  boolean getStrengthened();

  void setStrengthened(boolean strengthened);

}
