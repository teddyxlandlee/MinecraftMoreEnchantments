package stone926.mods.more_enchantments.interfaces;

import java.util.UUID;

public interface IAreaEffectCloudEntityFilterEntity {

  UUID getEntityDoNotAffect();

  void setEntityDoNotAffect(UUID e);

}
