package stone926.mods.more_enchantments.interfaces;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;

import java.util.UUID;

public interface IPersistentProjectileEntity {

  boolean isFromEnch();

  void setFromEnch(boolean b);

  int getDisappearCountdown();

  void setDisappearCountdown(int ticks);

  UUID getEntityDamage();

  void setEntityDamage(UUID uuid);

  UUID getEntityNotDamage();

  void setEntityNotDamage(UUID uuid);

  int getTraction();

  void setTraction(int traction);

  void setForceDamage(boolean forceDamage);

  boolean shouldForceDamage();

  default boolean tryForceDamage(Entity entity, DamageSource source, float amount) {
    boolean b;
    if (entity instanceof LivingEntity livingEntity && shouldForceDamage()) {
      b = ((ILivingEntity) livingEntity).forceDamage(source, amount);
    } else b = entity.damage(source, amount);
    return b;
  }

}
