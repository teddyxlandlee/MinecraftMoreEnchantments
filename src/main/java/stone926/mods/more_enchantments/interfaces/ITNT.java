package stone926.mods.more_enchantments.interfaces;

import net.minecraft.world.explosion.Explosion;

public interface ITNT {

  int getPower();

  void setPower(int power);

  Explosion.DestructionType getDestructionType();

  void setDestructionType(Explosion.DestructionType type);

  boolean summonedByExplosionCreator();

  void setSummonedByExplosionCreator(boolean summonedByExplosionCreator);

}
