package stone926.mods.more_enchantments.interfaces;

public interface IWitch {

  boolean isStrengthened();

  void setStrengthened(boolean strengthened);

}
