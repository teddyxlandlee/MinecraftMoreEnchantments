package stone926.mods.more_enchantments.interfaces;

public interface IBlockCooldown {

  int getBlockCooldown();

  void setBlockCooldown(int blockCooldown);

}
