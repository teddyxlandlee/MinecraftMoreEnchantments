package stone926.mods.more_enchantments.interfaces;

public interface IShulkerEntity {

  int getLevitationAmplifier();

  void setLevitationAmplifier(int levitationAmplifier);

  boolean levitationAmplifierChanged();

}
