package stone926.mods.more_enchantments.interfaces;

public interface ILogCooldown {

  int getCornelLogCooldown();

  void setCornelLogCooldown(int logCooldown);

  default boolean canCornelLog() {
    return getCornelLogCooldown() == 0;
  }

  int getMandalaLogCooldown();

  void setMandalaLogCooldown(int logCooldown);

  default boolean canMandalaLog() {
    return getMandalaLogCooldown() == 0;
  }

}
