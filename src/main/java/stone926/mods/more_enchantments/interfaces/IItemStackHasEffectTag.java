package stone926.mods.more_enchantments.interfaces;

import net.minecraft.entity.effect.StatusEffectInstance;

import java.util.List;

public interface IItemStackHasEffectTag {

  List<StatusEffectInstance> getEffects();

  void setEffects(List<StatusEffectInstance> statusEffectInstances);

  boolean hasEffects();

  void addEffect(StatusEffectInstance effectInstance);

  void addEffects(List<StatusEffectInstance> effectInstances);

}
