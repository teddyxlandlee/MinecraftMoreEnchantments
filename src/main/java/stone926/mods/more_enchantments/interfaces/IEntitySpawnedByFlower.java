package stone926.mods.more_enchantments.interfaces;

public interface IEntitySpawnedByFlower {

  boolean isSpawnedByMandala();

  void setSpawnedByMandala(boolean b);

  default boolean avoidBurning() {
    return isSpawnedByMandala();
  }

}
