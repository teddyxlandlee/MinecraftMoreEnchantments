package stone926.mods.more_enchantments.interfaces;

public interface IGuardianEntity {

  void setWarmupTime(int warmUpTime);

  boolean warmupTimeChanged();

}
