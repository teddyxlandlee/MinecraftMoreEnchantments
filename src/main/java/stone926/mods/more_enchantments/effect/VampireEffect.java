package stone926.mods.more_enchantments.effect;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.AttributeContainer;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.server.world.ServerWorld;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.damage.StoneEntityDamageSource;
import stone926.mods.more_enchantments.interfaces.ILivingEntity;
import stone926.mods.more_enchantments.mixins.accessors.WorldAccessor;

import java.util.UUID;

public class VampireEffect extends StatusEffect {

  public VampireEffect() {
    super(StatusEffectCategory.HARMFUL, 0x7b17ec);
  }

  @Override
  public void applyUpdateEffect(LivingEntity entity, int amplifier) {
    UUID v = ((ILivingEntity) entity).getVampire();
    float amount = (amplifier + 1) / 1.5F;
    if (v != null && !entity.getEntityWorld().isClient) {
      Entity vampire = ((WorldAccessor) (entity.getEntityWorld())).invokeGetEntityLookup().get(v);
      if (vampire != null) {
        boolean damaged = entity.damage(StoneEntityDamageSource.vampireEffect(vampire), amount);
        if (damaged && vampire instanceof LivingEntity l) {
          l.heal(amount);
          if (vampire.getEntityWorld() instanceof ServerWorld server) {
            server.spawnParticles(
              MoreEnchantmentsMod.VAMPIRE_HEART, vampire.getX(), vampire.getY() + vampire.getEyeHeight(vampire.getPose()), vampire.getZ(),
              1, 0.1, 0.1, 0.1, 0
            );
          }
        }
        return;
      }
    }
    entity.damage(StoneEntityDamageSource.vampireEffect(entity), amount);
  }

  @Override
  public boolean canApplyUpdateEffect(int duration, int amplifier) {
    return true;
  }

  @Override
  public void onApplied(LivingEntity entity, AttributeContainer attributes, int amplifier) {
    super.onApplied(entity, attributes, amplifier);
  }

  @Override
  public void onRemoved(LivingEntity entity, AttributeContainer attributes, int amplifier) {
    super.onRemoved(entity, attributes, amplifier);
    ((ILivingEntity) entity).removeVampire();
  }

}
