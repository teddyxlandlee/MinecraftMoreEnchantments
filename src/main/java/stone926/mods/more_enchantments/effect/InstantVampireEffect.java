package stone926.mods.more_enchantments.effect;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.AttributeContainer;
import net.minecraft.entity.effect.InstantStatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import org.jetbrains.annotations.Nullable;
import stone926.mods.more_enchantments.damage.StoneEntityDamageSource;
import stone926.mods.more_enchantments.interfaces.ILivingEntity;
import stone926.mods.more_enchantments.mixins.accessors.WorldAccessor;

import java.util.UUID;

public class InstantVampireEffect extends InstantStatusEffect {

  public InstantVampireEffect() {
    super(StatusEffectCategory.HARMFUL, 0x5d17ec);
  }

  @Override
  public void applyInstantEffect(@Nullable Entity potionEntity, @Nullable Entity potionOwner, LivingEntity target, int amplifier, double proximity) {
    if (potionOwner != null) {
      float amount = (amplifier + 1) * 4F;
      boolean damaged = ((ILivingEntity) (target)).forceDamage(StoneEntityDamageSource.vampireEffect(potionOwner), amount);
      if (damaged && potionOwner instanceof LivingEntity l) l.heal(amount);
    }
  }

  @Override
  public void applyUpdateEffect(LivingEntity entity, int amplifier) {
    UUID v = ((ILivingEntity) entity).getVampire();
    float amount = (amplifier + 1) * 3F;
    if (v != null) {
      Entity vampire = ((WorldAccessor) (entity.getEntityWorld())).invokeGetEntityLookup().get(v);
      if (vampire != null) {
        boolean damaged = entity.damage(StoneEntityDamageSource.vampireEffect(vampire), amount);
        if (damaged && vampire instanceof LivingEntity l) l.heal(amount);
        return;
      }
    }
    entity.damage(StoneEntityDamageSource.vampireEffect(entity), amount);
  }

  @Override
  public boolean canApplyUpdateEffect(int duration, int amplifier) {
    return true;
  }

  @Override
  public void onApplied(LivingEntity entity, AttributeContainer attributes, int amplifier) {
    super.onApplied(entity, attributes, amplifier);
  }

  @Override
  public void onRemoved(LivingEntity entity, AttributeContainer attributes, int amplifier) {
    super.onRemoved(entity, attributes, amplifier);
  }

}
