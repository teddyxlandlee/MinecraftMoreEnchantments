package stone926.mods.more_enchantments.util.potion;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.potion.Potion;

import java.util.ArrayList;
import java.util.List;

public final class PotionList {

  public static Multimap<StatusEffect, Potion> POTION = HashMultimap.create();

  public static List<Potion> ADDED_POTIONS = new ArrayList<>();

}
