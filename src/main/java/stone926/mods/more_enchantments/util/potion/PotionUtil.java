package stone926.mods.more_enchantments.util.potion;

import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.potion.Potion;
import net.minecraft.util.Util;

import java.util.List;
import java.util.Random;

public final class PotionUtil {

  public static Potion selectRandomPotion(StatusEffect effect, Random r) {
    List<Potion> potions = PotionList.POTION.get(effect).stream().toList();
    return Util.getRandom(potions, r);
  }

  public static Potion selectRandomPotion(Random r) {
    return Util.getRandom(PotionList.ADDED_POTIONS, r);
  }

}
