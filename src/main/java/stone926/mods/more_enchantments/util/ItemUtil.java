package stone926.mods.more_enchantments.util;

import net.minecraft.item.*;
import net.minecraft.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class ItemUtil {

  public static final List<ArmorItem> HELMETS = new ArrayList<>();
  public static final List<ArmorItem> CHESTPLATES = new ArrayList<>();
  public static final List<ArmorItem> LEGGINGS = new ArrayList<>();
  public static final List<ArmorItem> BOOTS = new ArrayList<>();

  public static final List<SwordItem> SWORDS = new ArrayList<>();
  public static final List<AxeItem> AXES = new ArrayList<>();

  public static final List<ShovelItem> SHOVELS = new ArrayList<>();
  public static final List<HoeItem> HOES = new ArrayList<>();
  public static final List<PickaxeItem> PICKAXES = new ArrayList<>();

  public static ToolItem getRandomTool(Random random) {
    return Util.getRandom(new ArrayList<>() {{ addAll(SHOVELS); addAll(HOES); addAll(PICKAXES); }}, random);
  }

  public static ToolItem getRandomWeapon(Random random) {
    return Util.getRandom(new ArrayList<>() {{ addAll(AXES); addAll(SWORDS); }}, random);
  }

  public static ArmorItem getRandomHelmet(Random random) { return Util.getRandom(HELMETS, random); }

  public static ArmorItem getRandomChestplate(Random random) { return Util.getRandom(CHESTPLATES, random); }

  public static ArmorItem getRandomLeggings(Random random) { return Util.getRandom(LEGGINGS, random); }

  public static ArmorItem getRandomBoots(Random random) { return Util.getRandom(BOOTS, random); }

}
