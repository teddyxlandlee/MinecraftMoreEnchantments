package stone926.mods.more_enchantments.util;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeInstance;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.mob.*;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.FishEntity;
import net.minecraft.item.ItemStack;
import stone926.mods.more_enchantments.mixins.accessors.MobEntityAccessor;

public final class EntityUtil {

  public static boolean isAnimal(Entity e) {
    return e instanceof AnimalEntity;
  }

  public static boolean isIllager(Entity e) {
    return e instanceof LivingEntity l
      && (l.getGroup() == EntityGroup.ILLAGER || l instanceof WitchEntity || l instanceof VexEntity);
  }

  public static boolean isRavager(Entity e) {
    return e instanceof RavagerEntity;
  }

  public static boolean isFish(Entity e) {
    return e instanceof FishEntity;
  }

  public static boolean isGuardian(Entity e) {
    return e instanceof GuardianEntity && !(e instanceof ElderGuardianEntity);
  }

  public static boolean isElderGuardian(Entity e) {
    return e instanceof ElderGuardianEntity;
  }

  public static boolean modifyAttribute(LivingEntity entity, EntityAttribute attr, EntityAttributeModifier modifier) {
    EntityAttributeInstance attributeInstance = entity.getAttributeInstance(attr);
    if (attributeInstance != null) {
      attributeInstance.addPersistentModifier(modifier);
      return true;
    } else {
      return false;
    }
  }

  public static void equipStackAndSetDropChance(MobEntity mob, EquipmentSlot slot, ItemStack stack, float dropChance) {
    mob.equipStack(slot, stack);
    mob.setEquipmentDropChance(slot, dropChance);
  }

  public static void addExperiencePoints(MobEntity mob, int additionalPoints) {
    MobEntityAccessor mobAccessor = (MobEntityAccessor) mob;
    mobAccessor.setExperiencePoints(mobAccessor.getExperiencePoints() + additionalPoints);
  }

}
