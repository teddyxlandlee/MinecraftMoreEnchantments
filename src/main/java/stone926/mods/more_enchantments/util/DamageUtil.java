package stone926.mods.more_enchantments.util;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.damage.StoneEntityDamageSource;
import stone926.mods.more_enchantments.enchantments.damage.AbstractStoneDamageEnchantment;
import stone926.mods.more_enchantments.enchantments.legendary.ThumpEnchantment;

import java.util.List;

public final class DamageUtil {

  public static boolean isDecapitation(DamageSource source) {
    return source instanceof StoneEntityDamageSource s && s.isDecapitation();
  }

  public static boolean isRetaliation(DamageSource source) {
    return source instanceof StoneEntityDamageSource s && s.isRetaliation();
  }

  public static boolean isDeath(DamageSource source) {
    return source instanceof StoneEntityDamageSource s && s.isDeath();
  }

  public static boolean isExtinction(DamageSource source) {
    return source instanceof StoneEntityDamageSource s && s.isExtinction();
  }

  private static float calcOtherDamageAmount(LivingEntity attacker, Entity en, float baseAmount) {
    if (en instanceof LivingEntity target) {
      List<AbstractStoneDamageEnchantment> enchantments = EnchantmentUtil.getStoneDamageEnchantments();
      float extraDamageAmount = 0;
      for (AbstractStoneDamageEnchantment e : enchantments) {
        int lvl = EnchantmentHelper.getEquipmentLevel(e, attacker);
        if (lvl > 0) {
          extraDamageAmount += e.getExtraDamageAmountForEntity(target, lvl);
        }
      }
      return extraDamageAmount;
    } else return 0;
  }

  public static float getExtraDamageAmount(LivingEntity attacker, Entity en, float baseAmount) {
    return calcOtherDamageAmount(attacker, en, baseAmount) + calcThumpDamageAmount(attacker, en, baseAmount);
  }

  public static float getTridentExtraDamageAmount(LivingEntity attacker, Entity en, float baseAmount) {
    return calcOtherDamageAmount(attacker, en, baseAmount) + calcTridentThumpDamageAmount(attacker, en, baseAmount);
  }

  private static float calcThumpDamageAmount(LivingEntity attacker, Entity en, float baseAmount) {
    if (en instanceof LivingEntity target) {
      float amount = 0;
      int thumpLvl = EnchantmentHelper.getEquipmentLevel(MoreEnchantmentsMod.THUMP, attacker);
      if (thumpLvl > 0) {
        amount += Math.min(45, ((target.getHealth() - baseAmount / 2.5F) * ThumpEnchantment.getExtraDamagePercentage(thumpLvl)));
      }
      StatusEffectInstance weak = attacker.getStatusEffect(StatusEffects.WEAKNESS);
      if (weak != null) amount *= (1 - (1 + weak.getAmplifier()) * 0.06);
      return amount;
    } else return 0;
  }

  private static float calcTridentThumpDamageAmount(LivingEntity attacker, Entity en, float baseAmount) {
    if (en instanceof LivingEntity target) {
      float amount = 0;
      int thumpLvl = EnchantmentHelper.getEquipmentLevel(MoreEnchantmentsMod.THUMP, attacker);
      if (thumpLvl > 0) {
        amount += Math.min(45, ((target.getHealth() - baseAmount / 2.5F) * ThumpEnchantment.getExtraDamagePercentage(thumpLvl)));
      }
      StatusEffectInstance weak = attacker.getStatusEffect(StatusEffects.WEAKNESS);
      if (weak != null) amount *= (1 - (1 + weak.getAmplifier()) * 0.06);
      return amount;
    } else return 0;
  }

}
