package stone926.mods.more_enchantments.util;

import net.minecraft.block.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.tag.BlockTags;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldView;
import stone926.mods.more_enchantments.MoreEnchantmentsMod;
import stone926.mods.more_enchantments.tags.MyBlockTags;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;

import static stone926.mods.more_enchantments.MoreEnchantmentsMod.FLOWER_ACTION_REPORT;

public final class BlockUtil {

  public static final Predicate<ServerWorld> SHOULD_FLOWER_LOG = world -> world.getGameRules().getBoolean(FLOWER_ACTION_REPORT);

  public static final List<StainedGlassBlock> STAINED_GLASS_BLOCKS = new ArrayList<>();
  public static final List<PaneBlock> PANE_BLOCKS = new ArrayList<>();
  public static final List<AbstractGlassBlock> GLASS_BLOCKS = new ArrayList<>(STAINED_GLASS_BLOCKS);
  public static final List<Block> WOOL_BLOCKS = new ArrayList<>();

  public static final List<Block> CORAL_BLOCKS = new ArrayList<>();
  public static final List<Block> DEAD_CORAL_BLOCKS = new ArrayList<>();
  public static final List<Block> CORALS = new ArrayList<>();
  public static final List<Block> DEAD_CORALS = new ArrayList<>();
  public static final List<Block> CORAL_FANS = new ArrayList<>();
  public static final List<Block> DEAD_CORAL_FANS = new ArrayList<>();

  public static Block getRandomCoralBlock(Random random) {
    return Util.getRandom(CORAL_BLOCKS, random);
  }

  public static Block getRandomDeadCoralBlock(Random random) {
    return Util.getRandom(DEAD_CORAL_BLOCKS, random);
  }

  public static Block getRandomCoralFans(Random random) {
    return Util.getRandom(CORAL_FANS, random);
  }

  public static Block getRandomDeadCoralFans(Random random) {
    return Util.getRandom(DEAD_CORAL_FANS, random);
  }

  public static Block getRandomCoral(Random random) {
    return Util.getRandom(CORALS, random);
  }

  public static Block getRandomDeadCoral(Random random) {
    return Util.getRandom(DEAD_CORALS, random);
  }

  public static AbstractGlassBlock getRandomGlassBlock(Random random) {
    return Util.getRandom(GLASS_BLOCKS, random);
  }

  public static PaneBlock getRandomPaneBlock(Random random) {
    return Util.getRandom(PANE_BLOCKS, random);
  }

  public static Block getRandomWoolBlock(Random random) {
    return Util.getRandom(WOOL_BLOCKS, random);
  }

  public static boolean mandalaAndCornelCanPlaceAt(BlockState state, WorldView world, BlockPos pos) {
    BlockPos down = pos.down();
    BlockState downState = world.getBlockState(down);
    return downState.isIn(BlockTags.SHOVEL_MINEABLE)
      || downState.isFullCube(world, down)
      || state.isIn(BlockTags.FIRE);
  }

  public static boolean canUseCompressedStonePickaxe(PlayerEntity player, BlockState state) {
    ItemStack stack = player.getMainHandStack();
    return stack.isOf(MoreEnchantmentsMod.COMPRESSED_STONE_PICKAXE)
      && !EnchantmentUtil.hasEnchantment(MoreEnchantmentsMod.DESTROY_CURSE, stack)
      && state.isIn(MyBlockTags.COMPRESSED_STONE_PICKAXE_MINEABLE);
  }

}
