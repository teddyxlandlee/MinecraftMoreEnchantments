package stone926.mods.more_enchantments.util;

import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.gen.StructureAccessor;
import net.minecraft.world.gen.feature.ConfiguredStructureFeature;

import java.util.Objects;

public final class StructurePredicate {

  /**
   * @param server      被检测的服务端世界
   * @param featureKeys 被探测是否存在的结构
   * @param x           x坐标
   * @param y           y坐标
   * @param z           z坐标
   * @return 世界上指定坐标的位置是否存在指定结构（大范围，探测小范围可以用LocationPredicate）
   */
  public static boolean test(ServerWorld server, RegistryKey<ConfiguredStructureFeature<?, ?>> featureKeys, double x, double y, double z) {
    Objects.requireNonNull(server, "param `server` must not be NULL");
    Objects.requireNonNull(featureKeys, "param `featureKeys` must not be NULL");
    StructureAccessor structureAccessor = server.getStructureAccessor();
    ConfiguredStructureFeature<?, ?> feature = structureAccessor
      .method_41036()
      .get(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY)
      .get(featureKeys);
    return feature != null && structureAccessor.getStructureAt(new BlockPos(x, y, z), feature).hasChildren();
  }

}
